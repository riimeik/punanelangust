Loo järgmised failid:
   * *.env.local*: sinna lähevad keskkonnamuutujad *APP_ENV=local*, *APP_URL=\<nt https://langust.local\>*, *DB_URL=\<nt mysql://user:pass@host:3306/dbname\>*
   * *.env.credentials*: sinna lähevad muutujad *REMOTE_DB_URL (seda kasutab run-skript andmebaasi pushimiseks ja pullimiseks)*, *FACEBOOK_APP_ID*, *FACEBOOK_APP_SECRET*