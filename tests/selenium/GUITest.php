<?php

use Facebook\WebDriver\Exception\NoSuchElementException;
use Facebook\WebDriver\Remote\DesiredCapabilities;
use Facebook\WebDriver\Remote\RemoteWebDriver;
use Facebook\WebDriver\WebDriverBy;

class GUITest extends PHPUnit_Framework_TestCase {
    protected static $driver;

    public static function setUpBeforeClass() {
        parent::setUpBeforeClass();
        self::$driver = RemoteWebDriver::create('http://localhost:4444/wd/hub', DesiredCapabilities::firefox());

        //self::$driver->findElement()->te
    }

    public static function tearDownAfterClass() {
        //self::$driver->close();
        parent::tearDownAfterClass();
    }

    public function setUp() {
        self::$driver->get(getenv('APP_URL'));
    }

    public function testViewProjects() {
        $that = $this;
        $d = self::$driver;


        $element = $d->findElement(WebDriverBy::cssSelector('a[href="/project"]'));
        if ($element->isDisplayed()) {
            $element->click();
        }else{
            $d->findElement(WebDriverBy::className("navbar-toggle"))->click();
            usleep(500000);
            $element->click();
        }
        
        $d->wait(3, 1000)->until(function($d) use($that) {
            try {
                $name = $d->findElement(WebDriverBy::cssSelector('tr#xslt-autoload-0 td:nth-child(1)'))->getText();
                $that->assertEquals('Teine projekt', $name);
                
                return true;
            } catch(NoSuchElementException $e) {
                return false;
            }
        });
    }
    //$this->webDriver->wait(10, 300)->until(WebDriverExpectedCondition::presenceOfElementLocated(WebDriverBy::name('username')));
    /*public function testMinuProov(){

        $d = self::$driver;
        $d->findElement(WebDriverBy::className("navbar-toggle"))->click();
        $d->findElement(WebDriverBy::cssSelector('a[href="/login"]'))->click();
        $d->findElement(WebDriverBy::className("btn-facebook"))->click();

    }*/
    public function testLogiSisseMeiliga(){
        $d = self::$driver;

            $element = $d->findElement(WebDriverBy::cssSelector('a[href="/login"]'));
            if ($element->isDisplayed()) {
                // do something...
                $d->findElement(WebDriverBy::cssSelector('a[href="/login"]'))->click();
            }else{
                $d->findElement(WebDriverBy::className("navbar-toggle"))->click();
                usleep(500000);
                $d->findElement(WebDriverBy::cssSelector('a[href="/login"]'))->click();
            }

            $d->findElement(WebDriverBy::name("email"))->sendKeys('test1@test.com');
            $d->findElement(WebDriverBy::name("password"))->sendKeys('lanGust7');
            $d->findElement(WebDriverBy::className("btn-success"))->click();
            $el = $d->findElement(WebDriverBy::className("navbar-toggle"));
            if($el->isDisplayed()) {
                $el->click();
                usleep(500000);
            }
            $mart = $d->findElement(WebDriverBy::cssSelector('a[href="/account"]'))->getText();
            $this->assertEquals('Mart Pihlakas', $mart);



    }
    public function testOtsiProjekt(){

        $d = self::$driver;

            $element = $d->findElement(WebDriverBy::cssSelector('a[href="/project/search"]'));
            if ($element->isDisplayed()) {
                $d->findElement(WebDriverBy::cssSelector('a[href="/project/search"]'))->click();
            }else{
                $d->findElement(WebDriverBy::className("navbar-toggle"))->click();
                usleep(500000);
                $d->findElement(WebDriverBy::cssSelector('a[href="/project/search"]'))->click();
            }

            $d->findElement(WebDriverBy::name("title"))->sendKeys('odav');
            $d->findElement(WebDriverBy::className("otsi_button"))->click();
            $elemendid = $d->findElements(WebDriverBy::className('hoverTable'));
            $name= $d->findElement(WebDriverBy::className('hoverTable'))->getAttribute('Pealkiri');

            //$name = $d->findElement(WebDriverBy::cssSelector('tr#portfolio-item td:nth-child(0)'))->getText();
        $name = $d->findElement(WebDriverBy::className('portfolio-item'))->getText();
            $this->assertEquals('Odav projekt 10 € 25 € Mart Pihlakas', $name);


    }

    public function testLisaProjekt(){
        $d = self::$driver;

            $element = $d->findElement(WebDriverBy::cssSelector('a[href="/account"]'));
            if ($element->isDisplayed()) {
                $element->click();
            }else{
                $d->findElement(WebDriverBy::className("navbar-toggle"))->click();
                usleep(500000);
                $d->findElement(WebDriverBy::cssSelector('a[href="/account"]'))->click();
            }

            $d->findElement(WebDriverBy::cssSelector('a[href="/project/myprojects"]'))->click();
            $d->findElement(WebDriverBy::cssSelector('a[href="#collapseOne1"]'))->click();
            $d->findElement(WebDriverBy::name("projectName"))->sendKeys('Automaattest');
            $d->findElement(WebDriverBy::name("kuupäev"))->sendKeys('28/05/2016');
            $d->findElement(WebDriverBy::name("kuupäev3"))->sendKeys('16/06/2016');
            $d->findElement(WebDriverBy::name("hindMin"))->click(); //klikib vales kohas (elemendi keskel lihtsalt)
            $d->findElement(WebDriverBy::name("hindMax"))->click(); //see ka klikib ns elemendi keskel
            $d->findElement(WebDriverBy::name("description"))->sendKeys('Siin on kirjas lisa mingi lisakirjeldus. dslkkljsdfljkdslaksdjsadfljkadskljaldfjk')->submit();

            //põmst võiks kontrollida ka, kas see projekt lisati
            //mine otsingusse, otsi enda projekti, kontrolli välja
            //või teoorias võiks see projekt sinu projketid lehele tekkida
        $el = $d->findElement(WebDriverBy::className("navbar-toggle"));
        if($el->isDisplayed()) {
            $el->click();
            usleep(500000);
        }
            $d->findElement(WebDriverBy::cssSelector('a[href="/project/search"]'))->click();
            $d->findElement(WebDriverBy::name("title"))->sendKeys('Automaattest');
            $d->findElement(WebDriverBy::className("otsi_button"))->click();
            $elemendid = $d->findElements(WebDriverBy::className('hoverTable'));
            //$name= $d->findElement(WebDriverBy::className('hoverTable'))->getAttribute('Pealkiri');
            //$name = $d->findElement(WebDriverBy::cssSelector('tr#portfolio-item td:nth-child(0)'))->getText();
            //$this->assertEquals('Odav projekt', $name);


    }
    public function testLogiV2lja(){
        $d = self::$driver;

            $element = $d->findElement(WebDriverBy::cssSelector('a[href="/account"]'));
            if ($element->isDisplayed()) {
                $element->click();
                $d->findElement(WebDriverBy::cssSelector('a[href="/logout"]'))->click();
            }else{
                $d->findElement(WebDriverBy::className("navbar-toggle"))->click();
                usleep(500000);
                $element->click();

               usleep(500000);

               $d->findElement(WebDriverBy::className('navbar-collapse'))->sendKeys(\Facebook\WebDriver\WebDriverKeys::ARROW_DOWN);

                $d->findElement(WebDriverBy::cssSelector('a[href="/logout"]'))->click();
                usleep(500000);
                $d->findElement(WebDriverBy::className("navbar-toggle"))->click();
                usleep(500000);
            }
            $logi = $d->findElement(WebDriverBy::cssSelector('a[href="/login"]'))->getText();
        usleep(500000);
            $regi = $d->findElement(WebDriverBy::cssSelector('a[href="/login/facebook"]'))->getText();
            $this->assertEquals(['Logi sisse','Registeeri'], array($logi, $regi));




    }
    public function testVahetameKeeli(){
        $d = self::$driver;

            $element = $d->findElement(WebDriverBy::cssSelector('a[href="#"]'));

            if ($element->isDisplayed()) {
                $element->click();
                $d->findElement(WebDriverBy::cssSelector('a[href="?lang=en"]'))->click();
            }else{
                $d->findElement(WebDriverBy::className("navbar-toggle"))->click();
                usleep(500000);
                $element->click();
                $d->findElement(WebDriverBy::cssSelector('a[href="?lang=en"]'))->click();
            }
          //  $d->findElement(WebDriverBy::cssSelector('a[href="#"]'))->click();
           // $d->findElement(WebDriverBy::cssSelector('a[href="?lang=en"]'))->click();
            $pealkiri = $d->findElement(WebDriverBy::className('center'))->getText();
            $this->assertEquals('Most popular projects', $pealkiri);




    }
   public function testAlgeksTagasi(){
       $d = self::$driver;
       $element = $d->findElement(WebDriverBy::cssSelector('a[href="#"]'));
       if ($element->isDisplayed()) {
           $element->click();
           $d->findElement(WebDriverBy::cssSelector('a[href="?lang=et"]'))->click();
       }else{
           $d->findElement(WebDriverBy::className("navbar-toggle"))->click();
           usleep(500000);
           $element->click();
           $d->findElement(WebDriverBy::cssSelector('a[href="?lang=et"]'))->click();
       }
       $d->findElement(WebDriverBy::className('navbar-brand'))->click();


   }

}