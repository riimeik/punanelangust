//J Liivi 2 koodrinaadid
var myCenter=new google.maps.LatLng(58.378417,26.714641);


function initialize()
{
    var mapProp = {
        center:myCenter,
        zoom:13,
        mapTypeId:google.maps.MapTypeId.ROADMAP

    };

    var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

    var marker=new google.maps.Marker({
        position:myCenter,
    });

    marker.setMap(map);

    //klikates 'markerile' suumitakse sisse
    google.maps.event.addListener(marker,'click',function() {
        map.setZoom(18);
        map.setCenter(marker.getPosition());
    });

    google.maps.event.addListener(map,'center_changed',function() {
// 3 seconds after the center of the map has changed, pan back to the marker
        window.setTimeout(function() {
            map.panTo(marker.getPosition());
        },3000);
    });
}


google.maps.event.addDomListener(window, 'load', initialize);

