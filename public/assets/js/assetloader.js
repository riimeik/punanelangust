function loadWithFallthrough(paths, cb) {
    cb = cb || function() {};

    (function setupElement() {
        var path = (paths.length ? paths.shift() : false);

        if(!path) {
            return cb(true);
        }

        console.log('[?] attempting to load ' + path);

        var element = document.createElement('script');
        element.setAttribute('src', path);

        element.onerror = function(e) {
            console.log('[-] failed to load ' + path);
            document.body.removeChild(element);
            setupElement();
        };

        element.onload = function(e) {
            console.log('[+] successfully loaded ' + path);
            cb(false);
        };

        document.body.appendChild(element);
    })();
}

function loadInBulk(assets) {
    (function loadNextGroup() {
        if(assets.length == 0) {
            return;
        }
        
        var loaded = 0;
        
        assets.shift().forEach(function(paths) {
            loaded--;
            
            loadWithFallthrough(paths, function(err) {
                if(++loaded == 0) {
                    loadNextGroup();
                }
            })
        });
    })();
}