function initSocketIOConnection(authToken) {
    $(document).ready(function() {
        var socket = io('punanewsserver.herokuapp.com?authToken=' + authToken);

        socket.on('messageCount', function(x) {
            $('#message-count span').fadeOut(function() {
                $(this).parent().addClass('active');
                $(this).text(x).fadeIn();
            });
        });

        socket.on('test', function() {
            console.log('Notification test!');
        });
    });
}

if(notificationURL) {
    initSocketIOConnection(notificationURL);
}