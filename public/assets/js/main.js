function displayAlerts(alerts) {
	if(!alerts) {
		return;
	}

	var types = [
		{name: 'success', class: 'success'},
		{name: 'warning', class: 'danger'}
	];

	$('#alerts').slideUp('fast', function() {
		$(this).empty();

		types.forEach(function(type) {
			if(alerts[type.name]) {
				alerts[type.name].forEach(function (alert) {
					var elem = $('<div class="alert alert-' + type.class + ' text-center">').html(alert);
					$('#alerts').append(elem);
				});
			}
		});

		$(this).slideDown();
	});
}

function loadXSLT(xmlurl, xslname, cb, fail) {
	var XML;
	var XSL;

	$.ajax({
		method: 'GET',
		url: xmlurl,
		data: 'json=true',
		cache: false
	}).done(function(response) {
		XML = $.parseXML(response.xml);
		merge();
	}).fail(function() {
		fail();
	});

	$.ajax({
		method: 'GET',
		url: '/assets/xsl/' + xslname + '.xsl',
		cache: true
	}).done(function(response) {
		XSL = response;
		merge();
	}).fail(function() {
		fail();
	});

	var merge = function() {
		if(XML && XSL) {
			var processor = new XSLTProcessor();
			processor.importStylesheet(XSL);
			var fragment = processor.transformToFragment(XML, document);

			if(cb) {
				cb(fragment);
			}
		}
	}
}

var Storage= window.sessionStorage;

function loadProjects(start, end, cb) {
	var hasCalled = false;

	loadXSLT('?start=' + start + '&end=' + end, 'project_all', function (frag) {
		if(hasCalled) {
			return;
		}

		hasCalled = true;

		var hasRows = false;
		var tables = frag.querySelectorAll('table');

		for (var i = 0; i < tables.length; i++) {
			var rows = tables[i].querySelectorAll('tr');

			if (rows.length > 0) {
				$(rows[0]).addClass('xslt-autoload').attr('id', 'xslt-autoload-' + start);
			}

			var rowGroup = [];

			for (var j = 0; j < rows.length; j++) {
				hasRows = true;
				$(rows[j]).addClass('xslt-autoload-sub');

				var rowWrapper = document.createElement('tbody');
				rowWrapper.appendChild(rows[j]);
				rowGroup.push(rowWrapper.innerHTML);

				$('#project_list > tbody').append(rows[j]);
			}

			Storage.setItem('xslt-autoload-' + start, JSON.stringify(rowGroup));

			start++;
		}

		if(cb) {
			cb(hasRows);
		}
	}, function() {
		if(hasCalled) {
			return;
		}

		hasCalled = true;

		var hasRows = false;

		for(var i = start; i < end; i++) {
			var rows;

			try {
				rows = JSON.parse(Storage.getItem('xslt-autoload-' + i));

				rows.forEach(function(row) {
					hasRows = true;
					$('#project_list > tbody').append(row);
					console.log('loaded project id ' + i + ' from session storage');
				});
			} catch(e) {
				console.log('no storage for project id ' + i);
			}
		}

		if(cb) {
			cb(hasRows);
		}
	});
}

function getLastAutoloadedRow() {
	try {
		var id = parseInt($('.xslt-autoload:last').attr('id').split('-')[2]);

		if(!isNaN(id) && isFinite(id)) {
			return id;
		}
	} catch(e) {
	}

	return false;
}

function getIntHash() {
	var hash = parseInt(window.location.hash.substr(1), 10);
	return !isNaN(hash) && isFinite(hash) && hash > 0 ? hash : false;
}

function loadProjectsByHash() {
	var lastAutoloadedRow = getLastAutoloadedRow();
	var hash = getIntHash();

	var start = (lastAutoloadedRow !== false ? lastAutoloadedRow : -1) + 1;
	var end = (hash !== false ? hash : 0);

	console.log('from ' + start + ' to ' + (end - 1));

	var deleteRows = function(hasRows) {
		var removing = false;

		$('.xslt-autoload-sub').each(function() {
			var id = false;

			try {
				id = $(this).attr('id').split('-')[2];
			} catch(e) {
			}

			if(id !== false && id > (end - 1)) {
				removing = true;
			}

			if(removing) {
				$(this).remove();
			}
		});

		$('#load-projects').prop('disabled', !hasRows && !removing);
	};

	if(end > start) {
		loadProjects(start, end, deleteRows);
	} else {
		deleteRows(false);
	}
}

$(window).load(function () {
	$('#load-projects').click(function() {
		var lastAutoloadedRow = getLastAutoloadedRow();
		var newRowCount = (lastAutoloadedRow !== false ? lastAutoloadedRow + 1 : 0) + 5;

		window.location.hash = '#' + newRowCount;
	});

	$(window).on('hashchange', loadProjectsByHash);

	if(!getIntHash()) {
		$('#load-projects').trigger('click');
	} else {
		loadProjectsByHash();
	}

	$('#submit-bid-inline').submit(function(e) {
		$.ajax({
			method: 'POST',
			cache: 'false',
			data: $(this).serialize() + '&json=true'
		}).done(function(response) {
			if(response.redirect) {
				return window.location.href = response.redirect;
			}

			$('#submit-bid-inline').trigger('reset');

			var bidderRow = $('#bidder-row-' + response.meta.id);
	
			if(!bidderRow.length) {
				bidderRow = $('#bidders-list tbody tr').first().clone();
				bidderRow.removeClass('hidden');
				bidderRow.attr('id', 'bidder-row-' + response.meta.id);

				$('#bidders-list tbody').prepend(bidderRow);
			}

			response.row.forEach(function(cell, i) {
				bidderRow.find('td:nth-child(' + (i + 1) + ')').text(cell);
			});

			$('#bidders-list').removeClass('hidden');
			$('#no-bidders-message').addClass('hidden');

			displayAlerts(response.alerts)
		});

		e.preventDefault();
	});

	$('.choose-winner').click(function() {
		$('.confirm-winner').each(function() {
			$(this).parent('div').slideUp();
			$(this).parent('div').prev().slideDown();
		});

		$(this).parent('div').slideUp();
		$(this).parent('div').next().slideDown();
	});

	$('.confirm-winner').click(function() {
		var id = $(this).closest('tr').attr('id').split('-')[2];

		$('#accept-winner input[name="bid_id"]').val(id);
		$('#accept-winner').submit();
	});

	$('#transaction-deposit').submit(function(e) {
		e.preventDefault();
		
		var service = $('#transaction-deposit select[name="service"] > option:selected').val(),
			id = 1,
			amount = $('#transaction-deposit input[name="amount"]').val(),
			message = 'Account deposit';

		$.ajax({
			method: 'GET',
			url: '/banklink/form',
			data: 'service=' + service + '&id=' + id + '&amount=' + amount + '&message=' + message,
			cache: false
		}).done(function(response) {
			$('body').append(response);
			$(response).submit();
		}).fail(function() {
			console.log('banklink form error');
		});
	});

	$('#idcard-login').click(function(e) {
		e.preventDefault();

		window.hwcrypto.use(null);

		window.hwcrypto.getCertificate({lang: 'en'}).then(function(cert) {
			window.hwcrypto.sign(
				cert, {
					type: 'SHA-256',
					hex: $('#idcard-login textarea[name="challenge"]').text()
				}, {
					lang: 'en'
				}
			).then(function(res) {
				$('#idcard-login').attr('action', '/login/idcard');
				$('#idcard-login textarea[name="certificate"]').val(hexToPem(cert.hex));
				$('#idcard-login textarea[name="signature"]').val(hexToBase64(res.hex));
				$('#idcard-login').submit();
			}, function(e) {
				displayAlerts({warning: ['Allkirjasamine ebaõnnestus']});
				console.log(e);
			})
		}, function(e) {
			var hrefParts = window.location.href.split('/');

			if(hrefParts[0] != 'https:') {
				hrefParts[0] = 'https:';
				window.location.href = hrefParts.join('/');
			} else {
				displayAlerts({warning: ['Sertifikaadi laadimine ebaõnnestus']});
				console.log(e);
			}
		});
	});
});
