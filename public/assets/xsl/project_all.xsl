<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:pl="https://punanelangust.herokuapp.com">
    <xsl:output method="html"/>
    <xsl:template match="pl:projects">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="pl:project">
        <table>
            <tbody>
                <tr class="vertical-middle">
                    <td class="h3 project-title">
                        <a href="/project/{@id}"><xsl:value-of select="@title"/></a>
                    </td>
                    <td class="h4 budget"><xsl:value-of select="pl:price/pl:low"/> - <xsl:value-of select="pl:price/pl:high"/></td>
                    <td><strong><xsl:value-of select="pl:author"/></strong></td>
                    <td>
                        <xsl:variable name="active" select="pl:active"/>
                        <xsl:choose>
                            <xsl:when test="$active > 0">
                                <span class="date btn btn-xs btn-success"><xsl:value-of select="pl:dates/pl:ends"/></span>
                            </xsl:when>
                            <xsl:otherwise>
                                <span class="date btn btn-xs btn-danger"><xsl:value-of select="pl:dates/pl:ends"/></span>
                            </xsl:otherwise>
                        </xsl:choose>
                    </td>
                    <td><xsl:value-of select="pl:dates/pl:duration"/></td>
                </tr>
                <tr class="vertical-middle">
                    <td colspan="5">
                        <ul class="tag-cloud">
                            <xsl:apply-templates select="pl:tags/pl:tag"/>
                        </ul>
                    </td>
                </tr>
            </tbody>
        </table>
    </xsl:template>
    <xsl:template match="pl:tag">
        <li><a class="btn btn-xs btn-default" href="#"><xsl:value-of select="current()"/></a></li>
    </xsl:template>
</xsl:stylesheet>