-- MySQL dump 10.16  Distrib 10.1.10-MariaDB, for Win32 (AMD64)
--
-- Host: localhost    Database: punanelangust
-- ------------------------------------------------------
-- Server version	10.1.10-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Temporary table structure for view `_user_balance_sub`
--

DROP TABLE IF EXISTS `_user_balance_sub`;
/*!50001 DROP VIEW IF EXISTS `_user_balance_sub`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `_user_balance_sub` (
  `user` tinyint NOT NULL,
  `balance` tinyint NOT NULL,
  `out_count` tinyint NOT NULL,
  `in_count` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `banklink_data`
--

DROP TABLE IF EXISTS `banklink_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banklink_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service` varchar(100) NOT NULL,
  `our_private_key` text NOT NULL,
  `their_certificate` text NOT NULL,
  `account` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bid`
--

DROP TABLE IF EXISTS `bid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bid` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project` int(11) NOT NULL,
  `bidder` int(11) NOT NULL,
  `placed_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `price` int(11) NOT NULL,
  `argument` text,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uq_projectBidder` (`project`,`bidder`),
  KEY `project` (`project`) USING BTREE,
  KEY `bidder` (`bidder`) USING BTREE,
  CONSTRAINT `fk_bid_to_project_project` FOREIGN KEY (`project`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_bid_to_user_bidder` FOREIGN KEY (`bidder`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sender` int(11) NOT NULL,
  `recipient` int(11) NOT NULL,
  `content` text NOT NULL,
  `data` text NOT NULL,
  `sent_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `read_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(150) NOT NULL,
  `description` text NOT NULL,
  `creator` int(11) DEFAULT NULL,
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `bidding_duration` int(11) NOT NULL COMMENT 'days',
  `project_duration` int(11) NOT NULL COMMENT 'days',
  `price_low` int(11) NOT NULL,
  `price_high` int(11) NOT NULL,
  `winning_bid` int(11) DEFAULT NULL,
  `result_delivered_on` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `creator` (`creator`) USING BTREE,
  CONSTRAINT `fk_project_to_user_creator` FOREIGN KEY (`creator`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `project_bid`
--

DROP TABLE IF EXISTS `project_bid`;
/*!50001 DROP VIEW IF EXISTS `project_bid`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `project_bid` (
  `id` tinyint NOT NULL,
  `project` tinyint NOT NULL,
  `bidder` tinyint NOT NULL,
  `placed_on` tinyint NOT NULL,
  `price` tinyint NOT NULL,
  `argument` tinyint NOT NULL,
  `first_name` tinyint NOT NULL,
  `last_name` tinyint NOT NULL,
  `is_winning_bid` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `project_full`
--

DROP TABLE IF EXISTS `project_full`;
/*!50001 DROP VIEW IF EXISTS `project_full`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `project_full` (
  `winning_bid` tinyint NOT NULL,
  `result_delivered_on` tinyint NOT NULL,
  `id` tinyint NOT NULL,
  `title` tinyint NOT NULL,
  `description` tinyint NOT NULL,
  `creator` tinyint NOT NULL,
  `created_on` tinyint NOT NULL,
  `bidding_duration` tinyint NOT NULL,
  `project_duration` tinyint NOT NULL,
  `price_low` tinyint NOT NULL,
  `price_high` tinyint NOT NULL,
  `first_name` tinyint NOT NULL,
  `last_name` tinyint NOT NULL,
  `bidding_ends_on` tinyint NOT NULL,
  `active` tinyint NOT NULL,
  `bids` tinyint NOT NULL,
  `tags` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `project_tag`
--

DROP TABLE IF EXISTS `project_tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project_tag` (
  `project` int(11) NOT NULL,
  `tag` int(11) NOT NULL,
  UNIQUE KEY `uq_project_tag` (`project`,`tag`) USING BTREE,
  KEY `project_id` (`project`),
  KEY `tag_id` (`tag`),
  CONSTRAINT `fk_project_tag_to_project_project_id` FOREIGN KEY (`project`) REFERENCES `project` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_project_tag_to_tag_tag_id` FOREIGN KEY (`tag`) REFERENCES `tag` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `tag`
--

DROP TABLE IF EXISTS `tag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `transaction`
--

DROP TABLE IF EXISTS `transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from_user` int(11) DEFAULT NULL,
  `to_user` int(11) NOT NULL,
  `amount` decimal(10,2) NOT NULL,
  `fields` text,
  `confirmed_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `from_user` (`from_user`),
  KEY `to_user` (`to_user`),
  CONSTRAINT `fk_transaction_to_user_from_user` FOREIGN KEY (`from_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_transaction_to_user_to_user` FOREIGN KEY (`to_user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) DEFAULT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `admin` tinyint(1) NOT NULL DEFAULT '0',
  `auth_token` varchar(64) DEFAULT NULL COMMENT 'sha256',
  `created_on` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `user_authable`
--

DROP TABLE IF EXISTS `user_authable`;
/*!50001 DROP VIEW IF EXISTS `user_authable`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `user_authable` (
  `id` tinyint NOT NULL,
  `email` tinyint NOT NULL,
  `first_name` tinyint NOT NULL,
  `last_name` tinyint NOT NULL,
  `admin` tinyint NOT NULL,
  `auth_token` tinyint NOT NULL,
  `password_hash` tinyint NOT NULL,
  `access_token` tinyint NOT NULL,
  `pin` tinyint NOT NULL,
  `certificate` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `user_balance`
--

DROP TABLE IF EXISTS `user_balance`;
/*!50001 DROP VIEW IF EXISTS `user_balance`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `user_balance` (
  `user` tinyint NOT NULL,
  `balance` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `user_message`
--

DROP TABLE IF EXISTS `user_message`;
/*!50001 DROP VIEW IF EXISTS `user_message`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `user_message` (
  `id` tinyint NOT NULL,
  `sender` tinyint NOT NULL,
  `recipient` tinyint NOT NULL,
  `content` tinyint NOT NULL,
  `data` tinyint NOT NULL,
  `sent_on` tinyint NOT NULL,
  `read_on` tinyint NOT NULL,
  `sender_first_name` tinyint NOT NULL,
  `sender_last_name` tinyint NOT NULL,
  `recipient_first_name` tinyint NOT NULL,
  `recipient_last_name` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `user_overview`
--

DROP TABLE IF EXISTS `user_overview`;
/*!50001 DROP VIEW IF EXISTS `user_overview`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `user_overview` (
  `balance` tinyint NOT NULL,
  `id` tinyint NOT NULL,
  `email` tinyint NOT NULL,
  `first_name` tinyint NOT NULL,
  `last_name` tinyint NOT NULL,
  `admin` tinyint NOT NULL,
  `auth_token` tinyint NOT NULL,
  `total_projects` tinyint NOT NULL,
  `active_projects` tinyint NOT NULL,
  `total_messages` tinyint NOT NULL,
  `unread_messages` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Temporary table structure for view `user_transaction`
--

DROP TABLE IF EXISTS `user_transaction`;
/*!50001 DROP VIEW IF EXISTS `user_transaction`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `user_transaction` (
  `id` tinyint NOT NULL,
  `from_user` tinyint NOT NULL,
  `to_user` tinyint NOT NULL,
  `amount` tinyint NOT NULL,
  `fields` tinyint NOT NULL,
  `confirmed_on` tinyint NOT NULL,
  `from_first_name` tinyint NOT NULL,
  `from_last_name` tinyint NOT NULL,
  `to_first_name` tinyint NOT NULL,
  `to_last_name` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `userdata_facebook`
--

DROP TABLE IF EXISTS `userdata_facebook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userdata_facebook` (
  `user` int(11) NOT NULL,
  `access_token` varchar(256) NOT NULL,
  PRIMARY KEY (`user`),
  CONSTRAINT `fk_userdata_facebook_to_user_user` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userdata_idcard`
--

DROP TABLE IF EXISTS `userdata_idcard`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userdata_idcard` (
  `user` int(11) NOT NULL,
  `pin` varchar(11) NOT NULL,
  `certificate` text NOT NULL,
  PRIMARY KEY (`user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `userdata_login`
--

DROP TABLE IF EXISTS `userdata_login`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `userdata_login` (
  `user` int(11) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  PRIMARY KEY (`user`),
  CONSTRAINT `fk_userdata_login_to_user_user` FOREIGN KEY (`user`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping routines for database 'punanelangust'
--
/*!50003 DROP PROCEDURE IF EXISTS `bid_set` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `bid_set`(IN `in_project` INT, IN `in_bidder` INT, IN `in_price` INT, OUT `out_id` INT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN
	DECLARE bid_id INT;
    
    SELECT id INTO bid_id FROM bid WHERE project = in_project AND bidder = in_bidder;
    
    IF (bid_id > 0) THEN
    	UPDATE bid SET price = in_price WHERE project = in_project AND bidder = in_bidder;
        SET out_id = bid_id;
    ELSE
		INSERT INTO bid (project, bidder, price) VALUES (in_project, in_bidder, in_price);
    	SET out_id = LAST_INSERT_ID();
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `message_add` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `message_add`(IN `in_sender` INT, IN `in_recipient` INT, IN `in_content` TEXT, IN `in_data` TEXT, IN `out_id` INT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN
	INSERT INTO message(sender, recipient, content, data) VALUES (in_sender, in_recipient, in_content, in_data);
    SET out_id = LAST_INSERT_ID();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `message_read` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `message_read`(IN `in_id` INT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN
	UPDATE message SET read_on = NOW() WHERE id = in_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `project_set` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `project_set`(IN `in_id` INT, IN `in_title` VARCHAR(150), IN `in_description` TEXT, IN `in_creator` INT, IN `in_bidding_duration` INT, IN `in_project_duration` INT, IN `in_price_low` INT, IN `in_price_high` INT, OUT `out_id` INT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN
DECLARE project_id INT;
   
   SELECT id INTO project_id FROM project WHERE id = in_id;
   
   IF (project_id > 0) THEN
   	UPDATE project SET title = in_title, description = in_description, creator = in_creator, bidding_duration = in_bidding_duration, project_duration = in_project_duration, price_low = in_price_low, price_high = in_price_high WHERE id = project_id;
       SET out_id = project_id;
   ELSE
INSERT INTO project (title, description, creator, bidding_duration, project_duration, price_low, price_high) VALUES (in_title, in_description, in_creator, in_bidding_duration, in_project_duration, in_price_low, in_price_high);
       SET out_id = LAST_INSERT_ID();
   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `transaction_add` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `transaction_add`(IN `in_project` INT, IN `in_sum` DECIMAL(10,3), IN `in_fields` TEXT, OUT `out_id` INT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN
	INSERT INTO transaction (project, sum, fields) VALUES (in_project, in_sum, in_fields);
	SET out_id = LAST_INSERT_ID();
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `transaction_forward` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `transaction_forward`(IN `in_id` INT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN
	UPDATE transaction SET forwarded_on = NOW() WHERE id = in_id;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `userdata_set` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `userdata_set`(IN `in_user` INT, IN `in_access_token` VARCHAR(256), IN `in_password_hash` VARCHAR(60), IN `in_pin` VARCHAR(11), IN `in_certificate` TEXT)
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN
    DECLARE user_id INT;
    
    IF (in_access_token IS NOT NULL) THEN
		SELECT user INTO user_id FROM userdata_facebook WHERE user = in_user;
        
        IF (user_id > 0) THEN
    		UPDATE userdata_facebook SET access_token = in_access_token WHERE user = in_user;
   		ELSE
    		INSERT INTO userdata_facebook (user, access_token) VALUES (in_user, in_access_token);
    	END IF;
    END IF;
    
    IF (in_password_hash IS NOT NULL) THEN
    	SELECT user INTO user_id FROM userdata_login WHERE user = in_user;
    
   		IF (user_id > 0) THEN
    		UPDATE userdata_login SET password_hash = in_password_hash WHERE user = in_user;
    	ELSE
    		INSERT INTO userdata_facebook (user, password_hash) VALUES (in_user, in_password_hash);
        END IF;
    END IF;
    
    IF (in_pin IS NOT NULL AND in_certificate IS NOT NULL) THEN
    	SELECT user INTO user_id FROM userdata_idcard WHERE user = in_user;
    
   		IF (user_id > 0) THEN
    		UPDATE userdata_idcard SET pin = in_pin, certificate = in_certificate WHERE user = in_user;
    	ELSE
    		INSERT INTO userdata_idcard (user, pin, certificate) VALUES (in_user, in_pin, in_certificate);
        END IF;
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_auth_set` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `user_auth_set`(IN `in_user` INT, IN `in_auth_token` VARCHAR(64))
    MODIFIES SQL DATA
    SQL SECURITY INVOKER
BEGIN
	UPDATE user SET auth_token = in_auth_token WHERE id = in_user;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `user_set` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8mb4 */ ;
/*!50003 SET character_set_results = utf8mb4 */ ;
/*!50003 SET collation_connection  = utf8mb4_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE PROCEDURE `user_set`(IN `in_id` INT, IN `in_email` VARCHAR(100), IN `in_first_name` VARCHAR(50), IN `in_last_name` VARCHAR(50), OUT `out_id` INT)
BEGIN
DECLARE user_id INT;
   
   SELECT id INTO user_id FROM user WHERE id = in_id;
   
   IF (user_id > 0) THEN
   	UPDATE user SET email = in_email, first_name = in_first_name, last_name = in_last_name WHERE id = user_id;
       SET out_id = user_id;
   ELSE
INSERT INTO user (email, first_name, last_name) VALUES (in_email, in_first_name, in_last_name);
   	SET out_id = LAST_INSERT_ID();
   END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `_user_balance_sub`
--

/*!50001 DROP TABLE IF EXISTS `_user_balance_sub`*/;
/*!50001 DROP VIEW IF EXISTS `_user_balance_sub`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `_user_balance_sub` AS (select `transaction`.`from_user` AS `user`,(sum(`transaction`.`amount`) * -(1)) AS `balance`,count(0) AS `out_count`,0 AS `in_count` from `transaction` group by `transaction`.`from_user`) union (select `transaction`.`to_user` AS `user`,sum(`transaction`.`amount`) AS `balance`,0 AS `out_count`,count(0) AS `in_count` from `transaction` group by `transaction`.`to_user`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `project_bid`
--

/*!50001 DROP TABLE IF EXISTS `project_bid`*/;
/*!50001 DROP VIEW IF EXISTS `project_bid`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `project_bid` AS select `b`.`id` AS `id`,`b`.`project` AS `project`,`b`.`bidder` AS `bidder`,`b`.`placed_on` AS `placed_on`,`b`.`price` AS `price`,`b`.`argument` AS `argument`,`u`.`first_name` AS `first_name`,`u`.`last_name` AS `last_name`,if((`b`.`id` = `p`.`winning_bid`),1,0) AS `is_winning_bid` from ((`bid` `b` left join `user` `u` on((`b`.`bidder` = `u`.`id`))) left join `project` `p` on((`b`.`project` = `p`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `project_full`
--

/*!50001 DROP TABLE IF EXISTS `project_full`*/;
/*!50001 DROP VIEW IF EXISTS `project_full`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `project_full` AS select `p`.`winning_bid` AS `winning_bid`,`p`.`result_delivered_on` AS `result_delivered_on`,`p`.`id` AS `id`,`p`.`title` AS `title`,`p`.`description` AS `description`,`p`.`creator` AS `creator`,`p`.`created_on` AS `created_on`,`p`.`bidding_duration` AS `bidding_duration`,`p`.`project_duration` AS `project_duration`,`p`.`price_low` AS `price_low`,`p`.`price_high` AS `price_high`,`u`.`first_name` AS `first_name`,`u`.`last_name` AS `last_name`,(`p`.`created_on` + interval `p`.`bidding_duration` day) AS `bidding_ends_on`,((`p`.`created_on` + interval `p`.`bidding_duration` day) >= now()) AS `active`,(select count(0) from `project_bid` where (`project_bid`.`project` = `p`.`id`) group by `project_bid`.`project`) AS `bids`,(select group_concat(`t`.`name` separator ',') from (`project_tag` `pt` join `tag` `t` on((`pt`.`tag` = `t`.`id`))) where (`pt`.`project` = `p`.`id`)) AS `tags` from (`project` `p` join `user` `u` on((`p`.`creator` = `u`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `user_authable`
--

/*!50001 DROP TABLE IF EXISTS `user_authable`*/;
/*!50001 DROP VIEW IF EXISTS `user_authable`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `user_authable` AS select `u`.`id` AS `id`,`u`.`email` AS `email`,`u`.`first_name` AS `first_name`,`u`.`last_name` AS `last_name`,`u`.`admin` AS `admin`,`u`.`auth_token` AS `auth_token`,`ul`.`password_hash` AS `password_hash`,`uf`.`access_token` AS `access_token`,`ui`.`pin` AS `pin`,`ui`.`certificate` AS `certificate` from (((`user` `u` left join `userdata_login` `ul` on((`u`.`id` = `ul`.`user`))) left join `userdata_facebook` `uf` on((`u`.`id` = `uf`.`user`))) left join `userdata_idcard` `ui` on((`u`.`id` = `ui`.`user`))) where ((`ul`.`password_hash` is not null) or (`uf`.`access_token` is not null) or ((`ui`.`pin` is not null) and (`ui`.`certificate` is not null))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `user_balance`
--

/*!50001 DROP TABLE IF EXISTS `user_balance`*/;
/*!50001 DROP VIEW IF EXISTS `user_balance`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `user_balance` AS select `_user_balance_sub`.`user` AS `user`,sum(`_user_balance_sub`.`balance`) AS `balance` from `_user_balance_sub` where (`_user_balance_sub`.`user` is not null) group by `_user_balance_sub`.`user` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `user_message`
--

/*!50001 DROP TABLE IF EXISTS `user_message`*/;
/*!50001 DROP VIEW IF EXISTS `user_message`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `user_message` AS select `m`.`id` AS `id`,`m`.`sender` AS `sender`,`m`.`recipient` AS `recipient`,`m`.`content` AS `content`,`m`.`data` AS `data`,`m`.`sent_on` AS `sent_on`,`m`.`read_on` AS `read_on`,`u1`.`first_name` AS `sender_first_name`,`u1`.`last_name` AS `sender_last_name`,`u2`.`first_name` AS `recipient_first_name`,`u2`.`last_name` AS `recipient_last_name` from ((`message` `m` left join `user` `u1` on((`m`.`sender` = `u1`.`id`))) join `user` `u2` on((`m`.`recipient` = `u2`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `user_overview`
--

/*!50001 DROP TABLE IF EXISTS `user_overview`*/;
/*!50001 DROP VIEW IF EXISTS `user_overview`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY INVOKER */
/*!50001 VIEW `user_overview` AS select coalesce(`ub`.`balance`,0) AS `balance`,`u`.`id` AS `id`,`u`.`email` AS `email`,`u`.`first_name` AS `first_name`,`u`.`last_name` AS `last_name`,`u`.`admin` AS `admin`,`u`.`auth_token` AS `auth_token`,coalesce((select count(0) from `project_full` where (`project_full`.`creator` = `u`.`id`) group by `project_full`.`creator`),0) AS `total_projects`,coalesce((select count(0) from `project_full` where ((`project_full`.`creator` = `u`.`id`) and (`project_full`.`active` <> 0)) group by `project_full`.`creator`),0) AS `active_projects`,coalesce((select count(0) from `message` where ((`message`.`recipient` = `u`.`id`) and (`message`.`read_on` <> 0)) group by `message`.`recipient`),0) AS `total_messages`,coalesce((select count(0) from `message` where ((`message`.`recipient` = `u`.`id`) and isnull(`message`.`read_on`)) group by `message`.`recipient`),0) AS `unread_messages` from (`user` `u` left join `user_balance` `ub` on((`u`.`id` = `ub`.`user`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;

--
-- Final view structure for view `user_transaction`
--

/*!50001 DROP TABLE IF EXISTS `user_transaction`*/;
/*!50001 DROP VIEW IF EXISTS `user_transaction`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 SQL SECURITY DEFINER */
/*!50001 VIEW `user_transaction` AS select `t`.`id` AS `id`,`t`.`from_user` AS `from_user`,`t`.`to_user` AS `to_user`,`t`.`amount` AS `amount`,`t`.`fields` AS `fields`,`t`.`confirmed_on` AS `confirmed_on`,`u1`.`first_name` AS `from_first_name`,`u1`.`last_name` AS `from_last_name`,`u2`.`first_name` AS `to_first_name`,`u2`.`last_name` AS `to_last_name` from ((`transaction` `t` left join `user` `u1` on((`t`.`from_user` = `u1`.`id`))) join `user` `u2` on((`t`.`to_user` = `u2`.`id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-22 17:29:23
