@extends('base')
{{ setLangSource('myprojects') }}

@section('body')
    @include('alerts')
    <section>
        <div class="container">
            <div class="panel-group" id="accordion1">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1"
                               href="#collapseOne1">
                                {{ lang('new') }}
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne1" class="panel-collapse collapse">
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" method="post">
                                <div class="form-group">
                                    <label class="col-md-8">{{ lang('name') }}</label>
                                    <div class="col-md-8">
                                        <input class="form-control" name="projectName" type="text">
                                    </div>
                                </div>

                                <div class='form-group'>
                                    <label class="col-md-8">{{ lang('a_deadline') }}</label>
                                    <div class="col-md-8">

                                        <div class='input-group date' id='datetimepicker6' data-provide="datepicker">
                                            <input type='text' class="form-control" name="kuupäev"/>
                                                   <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                        </div>
                                    </div>
                                </div>

                                <div class='form-group'>
                                    <label class="col-md-8">{{ lang('s_deadline') }}</label>
                                    <div class="col-md-8">

                                        <div class='input-group date' id='datetimepicker7' data-provide="datepicker">
                                            <input type='text' class="form-control" name="kuupäev3"/>
                                                   <span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
                                        </div>
                                    </div>
                                </div>




                                <div class="form-group">
                                    <label class="col-md-8">{{ lang('tags') }}</label>

                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="tags" placeholder="{{ lang('tags-helper') }}" />
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label class="col-md-8 ">{{ lang('min') }}</label>
                                    <div class="col-md-8">
                                        <input class="form-control" id="hindMin" name="hindMin" type="range" min="0"
                                               max="1000" value="0" step="5"
                                               oninput="hind.value=hindMin.value">
                                        Hind:
                                        <output name="hind" id="hindMinOutput" for="hindMin">0</output>
                                        €
                                    </div>

                                    <label class="col-md-8 ">{{ lang('max') }}</label>
                                    <div class="col-md-8">
                                        <input class="form-control" id="hindMax" name="hindMax" type="range" min="0"
                                               max="1000" value="0" step="5"
                                               oninput="hind1.value=hindMax.value">
                                        Hind:
                                        <output name="hind1" id="hindMaxOutput" for="hindMax">0</output>
                                        €
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-8 ">{{ lang('info') }}</label>
                                    <div class="col-md-8">
                                        <input class="form-control" name="description" type="text">
                                        <!--<textarea class="form-control" rows="3" name="description"></textarea>-->
                                    </div>
                                </div>

                                <!-- <div class="form-group">
                                     <label class="col-md-7 ">Sisesta parool kinnitamiseks</label>
                                     <div class="col-md-7">
                                         <input class="form-control" type="password">
                                     </div>
                                 </div>-->
                                <div class="form-group">

                                    <div class="col-md-7">
                                        <input class="btn" value={{ lang('new') }} type="submit">
                                    </div>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section>
        <div class="container">

            <h2>{{ lang('my_p') }}</h2>

            <table class="table hoverTable project-list">
                <thead>
                <tr>
                    <th>{{ lang('name') }}</th>
                    <th>{{ lang('created') }}</th>
                    <th>{{ lang('a_deadline') }}</th>
                    <th>{{ lang('budget') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($riin as $pro)

                    <tr onclick="window.document.location='{{ url($pro['id']) }}';">
                        <td>{{$pro['title']}}</td>
                        <td>{{$pro['created_on']}}</td>

                        <td>{{ eeDate($pro['bidding_ends_on']) }}</td>
                        <td>{{$pro['price_low']}}€ - {{$pro['price_high']}}€</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            <h2>{{ lang('cand') }}</h2>
            <table class="table hoverTable project-list">
                <thead>
                <tr>
                    <th>{{ lang('name') }}</th>
                    <th>{{ lang('creator') }}</th>
                    <th>{{ lang('a_deadline') }}</th>
                    <th>{{ lang('time') }}</th>
                    <th>{{ lang('bid') }}</th>
                </tr>
                </thead>
                <tbody>
                @foreach($bids as $bid)
                    <tr onclick="window.document.location='{{ url($bid['id']) }}';">
                        <td>{{$bid['title']}}</td>
                        <td>{{$bid['first_name']}} {{$bid['last_name']}}</td>
                        <td>{{ eeDate($bid['bidding_ends_on']) }}</td>
                        <td>{{$bid['project_duration']}} {{lang('day')}}</td>
                        <td>{{$bid['price']}}€</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>

    </section>
@endsection