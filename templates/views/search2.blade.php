
@extends('base')
{{ setLangSource('search') }}
@section('body')
    <section>
        <div class="container">
            <div class="row searchi_background">

                <header class="center gap">
              <h2>{{ lang('search') }}</h2>
                    <span id='element-inline' class="glyphicon glyphicon-info-sign info"></span>
                    <div class='move-into-tooltip' style='display:none'>{{ lang('info') }}</div>
                    </header>
                    <form  method="post" >
                        <div class="panel panel-default">
                        <table class="table hoverTable">
                            <thead>
                            <tr class="form-group">
                                <th><input class="form-control" placeholder="{{ lang('title') }}" type="text" name="title" onkeyup="showResult(this.value)"></th>

                               <!-- <th><input class="form-control" placeholder="Tähtaeg" type="text" name="date"></th> -->
                                <th><input class="form-control" placeholder="{{ lang('min') }}" type="text" name="min_price"></th>
                                <th><input class="form-control" placeholder="{{ lang('max') }}" type="text" name="max_price"></th>

                                <th><input class="form-control" placeholder="{{ lang('first') }}" type="text" name="first_name"></th>
                                <th><input class="form-control" placeholder="{{ lang('last') }}" type="text" name="last_name"></th>


                            </tr>
                            <!--<th>Kasutajanimi </th><th>Tähtaeg</th><th>Hind</th><th>Kategooria</th>-->
                            </thead>
                            <tbody >

                            @if(isset($projektid2))

                            @foreach($projektid2 as $project)
                                <div class="portfolio-items col-3">
                                <tr class="portfolio-item" onclick="window.document.location='{{ url($project['id']) }}';" >
                                <td>{{ $project['title'] }} </td>
                                    <!--  <td>{{ $project['created_on'] }} </td>-->
                                    <td>{{ $project['price_low'] }} € </td>
                                    <td>{{ $project['price_high'] }} €</td>
                                    <td>{{ $project['first_name'] }} </td>
                                    <td>{{ $project['last_name'] }} </td>
                                </tr>
                                </div>

                             @endforeach
                                @endif

                            </tbody>
                        </table>
                        </div>
                        <div class="center"><button type="submit" class="btn btn-success btn-md otsi_button ">{{ lang('search2') }}</button>

                        </div>







                    </form>



        </div>
            </div>
    </section>


@endsection