@extends('base_old')

@section('body')
    <section>
        <h2 id="projekt_name">{{ $project['name'] }}</h2>
        <div class="container">
            <div class="row">
                <!--mobiili klassid panemata!!!-->
                <div class="col-md-3">
                    <img alt="projekti pilt" src="/assets/img/projects/{{ $project['img'] }}" />
                 </div>
                <div class="col-md-8">
                <table class="table">
                    <tr>
                        <td><strong>Projekti nimi(siia ka?):</strong></td>
                        <td>{{ $project['name'] }}</td>
                    </tr>
                    <tr>
                        <td><strong>Algataja:</strong></td>
                        <td>{{ $project['author'] }}</td>
                    </tr>
                    <tr>
                        <td><strong>Tähtaeg:</strong></td>
                        <td>{{ $project['projectDeadline'] }}</td>
                    </tr>
                    <tr>
                        <td><strong>Kandideerimise tähtaeg:</strong></td>
                        <td>{{ $project['biddingDeadline'] }}</td>
                    </tr>
                    <tr>
                        <td><strong>Kategooria:</strong></td>
                        <td>{{ $project['category'] }}</td>
                    </tr>
                    <tr>
                        <td><strong>Eelarve:</strong></td>
                        <td>{{ $project['budget'] }}€</td>
                    </tr>
                </table>
            </div>
            </div>
            <div class ="row">
                <div class="col-md-12">
                   <h3>Projecti tuvustus, jne</h3>
                    {{ $project['description'] }}
                </div>
            </div>
        </div>

    </section>
@endsection