@extends('base')

{{ setLangSource('home') }}

@section('body')
<div id="navbar-shadow"></div>

<section id="home-banner" class="jumbotron">
    <div class="container">
        <div class="row">
            <div class="col-md-9 visible-md visible-lg">
                <h1 class="">{{ lang('welcome') }}</h1>
                <p></p>
            </div>
            <div class="col-md-3">
                <div id="register-button" class="vertical-center text-center">
                    <a href="{{ url('/login/facebook') }}" class="btn btn-success btn-lg">{{ lang('register') }}</a>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="container" id="home-preview">
    <div class="center gap">
        <h2>{{ lang('popular') }}</h2>
    </div>
    <form class="hidden" method="post" action="{{ $banklink->getRequestUrl() }}">
        <button type="submit" class="button">BANKLINK TEST</button>
        {!! $banklink->buildRequestHtml() !!}
    </form>

    <div id="pricing-table" class="row">
    @foreach($projects as $project)
        <div class="col-md-4 col-xs-12">
            <ul class="plan plan2 featured">
                <li class="plan-name">
                    <h3>{{ $project['title'] }}</h3>
                </li>
                <li>
                    <strong>{{ lang('deadline') }}: </strong> {{ $project['bidding_ends_on'] }}
                </li>
                <li>
                    <strong>{{ lang('price') }}: </strong> {{ $project['price_low'] }}€ - {{ $project['price_high'] }}€
                </li>
                <li>
                    <ul class="tag-cloud">
                    @foreach(explode(',', $project['tags']) as $tag)
                        <li><a class="btn btn-xs btn-default" href="#">{{ $tag }}</a></li>
                    @endforeach
                    </ul>
                </li>
                <li class="plan-action">
                    <a href="{{ url('project/' . $project['id']) }}" class="btn btn-primary btn-md">{{ lang('more') }}</a>
                </li>
            </ul>
        </div>
    @endforeach
    </div>

    <div class="center gap gap">
        <a id="load-more" href="/project" class="btn btn-success" >{{ lang('all') }}</a>
    </div>
</div>

@endsection