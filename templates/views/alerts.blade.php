<div id="alerts" class="center">
    @foreach(getFlash('error') as $text)
        <div class="alert alert-danger text-center">{{ $text }}</div>
    @endforeach

    @foreach(getFlash('success') as $text)
        <div class="alert alert-success text-center">{{ $text }}</div>
    @endforeach
</div>