{{ setLangSource('navigation') }}

<header id="header" class="navbar navbar-inverse sininelangust">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/') }}"><img src="/assets/img/logo.png" alt="meielogo"></a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ url('/') }}"> {{ App::get('user')->hasPrivilege('USER') ? lang('overview') : lang('home') }}</a></li>
                <li><a href="{{ url('/project') }}">{{ lang('projects') }}</a></li>
                <li><a href="/project{{ url('/search') }}">{{ lang('search') }}</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ lang('language') }}</a>
                    <ul class="dropdown-menu">

                        <li><a href="?lang=en"><img src="/assets/img/flags/gb.png" alt="GBflag"/> {{ lang('en') }}</a></li>
                        <li><a href="?lang=et"><img src="/assets/img/flags/ee.png" alt="ESflag" /> {{ lang('et') }}</a></li>

                    </ul>
                </li>
                @if(App::get('user')->hasPrivilege('USER'))
                    <li class="empty">&nbsp;</li>
                    <li class="dropdown active left">
                        <a href="/account" class="dropdown-toggle" data-toggle="dropdown"><strong>{{ App::get('user')->getFullName() }}</strong></a>
                        <ul class="dropdown-menu">
                            <li><a href="{{ url('/profile') }}">{{ lang('profile') }}</a></li>
                            <li><a href="/project{{ url('/myprojects') }}">{{ lang('y_projects') }}</a></li>
                            <li><a href="{{ url('/message') }}">{{ lang('messages') }}</a></li>
                            <li><a href="{{ url('/transaction') }}">{{ lang('transactions') }}</a></li>
                            <li class="divider"></li>
                            <li><a href={{url("/logout")}}><span class="glyphicon glyphicon-remove-circle"></span>&nbsp;&nbsp;{{ lang('logout') }}</a></li>
                        </ul>
                    </li>
                    <li class="right login">
                        <a id="message-count" href="{{ url('/message') }}"><span>{{ App::get('user')->get('unread_messages') }}</span></a>
                    </li>
                @else
                    <li><a href="{{ url('/faq') }}">{{ lang('whereami') }}</a></li>
                    <li class="empty">&nbsp;</li>
                    <li class="login left"><a href="{{ url('/login') }}">{{ lang('login') }}</a></li>
                    <li class="register right"><a href="/login{{ url('/facebook') }}"><strong>{{ lang('reg') }}</strong></a></li>
                @endif
            </ul>
        </div>
    </div>
</header>