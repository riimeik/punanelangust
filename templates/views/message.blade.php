@extends('base')

{{ setLangSource('messages') }}

@section('body')
@include('alerts')
<div class="container">
    <h3 class="center gap">{{ lang('title') }}</h3>
    <div class="container">
        @foreach($messages as $message)
        <div class="alert alert-neutral row{{ $message['read_on'] ? ' read' : '' }}">
            <div class="col-md-3">
                {{ lang('author') }}: <strong>{{ $message['sender_first_name'] . ' ' . $message['sender_last_name'] }}</strong><br/>
                {{ lang('when') }}: <strong>{{ eeDate($message['sent_on']) }}</strong>@if($message['read_on']) <em>(loetud)</em>@else @endif
            </div>
            <hr class="visible-sm visible-xs" />
            <div class="col-md-9">
                <blockquote>
                    @if($message['data'])
                    {!! App::get('message')->renderFromTemplate($message['content'], unserialize($message['data'])) !!}
                    @else
                    {!! $message['content'] !!}
                    @endif
                </blockquote>
            </div>
        </div>
        @endforeach
        <h4 class="center{{ $messages ? ' hidden' : '' }}">Sulle ei ole hetkel ühtegi sõnumit :(</h4>
    </div>
</div>
@endsection