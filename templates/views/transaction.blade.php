@extends('base')

{{ setLangSource('transaction') }}

@section('body')
@include('alerts')
<div class="container">
    <h3 class="center gap"></h3>
    <div class="container">
        <div class="col-md-6">
            <h4 class="center gap">{{ lang('history') }}</h4>
        <table class="table vertical-middle transactions-list">
            <tr>
                <th></th>
                <th>{{ lang('sender') }}</th>
                <th>{{ lang('recipient') }}</th>
                <th>{{ lang('sum') }}</th>
                <th>{{ lang('date') }}</th>
            </tr>
            @foreach($transactions as $transaction)
            <tr>
                <td>
                    @if($transaction['from_user'] == App::get('user')->get('id'))
                    <span class="btn btn-info glyphicon glyphicon-chevron-left"></span>
                    @else
                    <span class="btn btn-warning glyphicon glyphicon-chevron-right"></span>
                    @endif
                </td>
                <td>
                    @if($transaction['from_user'])
                        {!! ($transaction['from_user'] == 1 ? '<em>' : '') . $transaction['from_first_name'] . ' ' . $transaction['from_last_name'] . ($transaction['from_user'] == 1 ? '</em>' : '') !!}
                    @else
                        <em>{{ lang('bank') }}</em>
                    @endif
                </td>
                <td>{{ $transaction['to_first_name'] . ' ' . $transaction['to_last_name']  }}</td>
                <td><strong class="text-success">{{ $transaction['amount'] }}€</strong></td>
                <td>{{ eeDate($transaction['confirmed_on']) }}</td>
            </tr>
            @endforeach
        </table>
        <h5 class="center{{ $transactions ? ' hidden' : '' }}">{{ lang('no-transactions') }}</h5>
        </div>
        <br />
        <hr class="hidden-md hidden-lg">
        <div class="col-md-6">
            <h3 class="center text-info">{{ lang('balance') }}: <strong>{{ App::get('user')->get('balance') }}€</strong></h3>
            <hr>
            <div class="center">
                {{ lang('bank-transaction') }}:<br /><br />
                <form id="transaction-deposit" method="post">
                <div class="row">
                    <div class="col-md-8">
                        <div class="input-group" style="margin-bottom: 10px">
                            <input name="amount" required="required" type="number" step="0.01" min="0" class="form-control" placeholder="{{ lang('insert-amount') }}" />
                            <div class="input-group-addon"> €</div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <select name="service" class="form-control" required="required">
                            <option disabled="disabled" selected="selected" value="">{{ lang('choose-bank') }}</option>
                            <option value="swedbank-legacy">Swedbank</option>
                            <option value="seb-legacy">SEB</option>
                            <option value="krediidipank-legacy">Krediidipank</option>
                            <option value="sampo-legacy">Danske</option>
                            <option value="nordea-legacy">Nordea</option>
                            <option value="lhv-legacy">LHV</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="center bid-button">
                            <button type="submit" class="btn btn-info btn-block">{{ lang('submit') }}</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <br /><br />
    </div>
</div>
@endsection