
@extends('base_old')


@section('body')

    <section id="portfolio" class="container">

        <div class="row">
        <div class="panel panel-primary filterable">
            <div class="panel-heading">
                <h3 class="panel-title">Projektid</h3>
                <div class="pull-right">
                    <button id="like" class="btn btn-default btn-s btn-filter"> Otsi</button>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="table-responsive">
        <div class="panel panel-default">
        <table class="table hoverTable">
            <thead>
            <tr class="filters">
                <th><input type="text" class="form-control" placeholder="Kasutajanimi" disabled></th>
                <th><input type="text" class="form-control" placeholder="Tähtaeg (pp.kk.aaaa)" disabled></th>
                <th><input type="text" class="form-control" placeholder="Hind" disabled></th>
                <th><input type="text" class="form-control" placeholder="Kategooria" disabled></th>

            </tr>
            <!--<th>Kasutajanimi </th><th>Tähtaeg</th><th>Hind</th><th>Kategooria</th>-->
            </thead>
            <tbody id="myTable">
                 @foreach($projects as $project)
                <div class="portfolio-items col-3">
                    <tr class="portfolio-item bootstrap wordpress" onclick="window.document.location='/projekt/1';" >
                        <td>{{ $project['author'] }} </td><td>  {{ $project['biddingDeadline'] }} </td><td>   {{ $project['budget'] }}  </td> <td>   {{ $project['category'] }}</td>
                    </tr>
                </div>
                @endforeach
            </tbody>
                </table>
        </div>
                        <div class="col-md-12 text-center">
                            <ul class="pagination" id="myPager"></ul>
                        </div>
                    </div>
                </div>

            </div>
</div>

            <div class="container">
                <div class="row">
                    <div class="table-responsive">

                    </div>
                    <div class="col-md-12 text-center">
                        <ul class="pagination" id="myPager"></ul>
                    </div>
                </div>
            </div>


            @endsection



