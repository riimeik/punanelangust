@extends('base')
{{ setLangSource('error') }}
@section('body')
    <section id="error" class="container">
        <h1 class="section-title">&#9785; {{ lang('error') }}</h1>
        @foreach(getFlash('error') as $message)
        <p>{{ $message }}</p>
        @endforeach
        <br />
        <a class="btn btn-success" href={{url('/')}}>{{ lang('back') }}</a>
    </section>
@endsection