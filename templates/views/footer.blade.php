
{{ setLangSource('footer') }}
<section id="footer" class="sininelangust">
    <div class="container">
        <div class="row">
            <div class="col-md-4 hidden-sm hidden-xs">
                <h4>{{lang('menu')}}</h4>
                <div>
                    <ul class="arrow">
                        <li><a href="{{url('/')}}">{{lang('home')}}</a></li>
                        <li><a href="{{url('/faq')}}">{{lang('where')}}</a></li>
                        <li><a href="{{url('/project/search')}}">{{lang('search')}}</a></li>
                        <li><a href="{{url('/project')}}">{{lang('projects')}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4 hidden-sm hidden-xs">
                <h4>{{lang('personal')}}</h4>

                @if(App::get('user')->hasPrivilege('USER'))
                <ul class="arrow">
                    <li><a href="{{url('/profile')}}"><strong>{{lang('profile')}}</strong></a></li>
                    <li><a href="{{url('/project/myprojects')}}">{{lang('myPro')}}</a></li>
                    <li><a href="{{url('/message')}}">{{lang('mes')}}</a></li>
                    <li><a href="{{url('/logout')}}">{{lang('logOut')}}</a></li>
                </ul>
                    @else
                    <ul class="arrow">
                        <li><a href="{{url('/login')}}"><strong>{{lang('register')}}</strong></a></li>
                        <li><a href="{{url('/login')}}">{{lang('login')}}</a></li>
                    </ul>
                    @endif
            </div>
            <div class="col-md-4 col-sm-12 center">
                <h4>Punane langust, 2016</h4>
                <div>Heidi&nbsp;Urmet,</div>
                <div>Krisseliine&nbsp;Pärt,</div>
                <div>Simo&nbsp;Pähk</div>
            </div>
        </div>
    </div>
</section>
