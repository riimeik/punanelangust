<?php
    $min = "";

    if(getenv('APP_PRODUCTION')) {
        $min = ".min";
    }
?>

<!DOCTYPE html>
<html lang="et">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{ isset($title) ? $title : "Punane langust" }}</title>

    <link rel="stylesheet" href="/assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="/assets/css/main.css">
    <script id="script-tag-jquery"></script>
    @yield('headers')
</head>
<body>


    @include('navigation')
    @yield('body')
    @include('footer')

    <script id="asdasd">
        var notificationURL = '{{ App::get('user')->hasPrivilege('USER') ? App::get('user')->get('id') . ':' . App::get('user')->get('auth_token') : '' }}';
        @if(getenv('DISABLE_PUSH_NOTIFICATIONS') == 'true')
                notificationURL = null;
        @endif

        function loadAlt(type, url) {
            if(type === 'undefined') {
                var elem = document.createElement('script');
                elem.setAttribute('src', url);
                document.body.appendChild(elem);
            }
        }
    </script>
    <script src="https://code.jquery.com/jquery-2.2.3.min.js"></script>
    <script src="https://cdn.socket.io/socket.io-1.4.5.js"></script>
    <script>
        loadAlt(typeof $, '/assets/js/jquery-2.2.3.min.js');
        loadAlt(typeof io, '/assets/js/socket.io-1.4.5.min.js');
    </script>
    <script src="/assets/js/bootstrap-3.3.6.min.js"></script>
    <script src="/assets/js/main{{ $min }}.js"></script>
    <script src="/assets/js/notifications{{ $min }}.js"></script>
    <script src="/assets/js/search{{ $min }}.js"></script>
    <script src="/assets/js/bootstrap-datepicker{{ $min }}.js"></script>
    <script src="/assets/js/tipped{{ $min }}.js"></script>
    <script src="/assets/js/readXml{{ $min }}.js"></script>
    @yield('scripts')

    <link rel="stylesheet" property="stylesheet" href="/assets/css/font-awesome.min.css">
    <link rel="stylesheet" property="stylesheet" href="/assets/css/animate.min.css">
    <link rel="stylesheet" property="stylesheet" href="/assets/css/datepicker.min.css">
    <link rel="stylesheet" property="stylesheet" href="/assets/css/tipped.min.css">
</body>
</html>