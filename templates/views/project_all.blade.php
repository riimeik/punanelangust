@extends('base')

{{ setLangSource('project') }}

@section('body')
@include('alerts')
<div class="container">
    <h2 class="center gap">{{ lang('bigTitle') }}</h2>
    <table id="project_list" class="table table-responsive panel-default project-list">
        <thead>
        <tr class="verical-middle">
            <th>{{ lang('title') }}</th>
            <th>{{ lang('budget') }}</th>
            <th>{{ lang('author') }}</th>
            <th>{{ lang('deadline') }}</th>
            <th>{{ lang('time') }}</th>
        </tr>
        </thead>
        <tbody>
        <!--
        <tr class="vertical-middle hidden">
            <td class="h3 project-title"><a href="#"></a></td>
            <td class="h4 budget"></div></td>
            <td><strong></strong></td>
            <td><span class="date btn btn-xs btn-success"></span><span class="date btn btn-xs btn-danger"></span></td>
            <td> päeva</td>
        </tr>
        <tr class="vertical-middle hidden">
            <td colspan="5">
                <ul class="tag-cloud">

                </ul>
            </td>
        </tr>
        @foreach($projects as $project)
        <tr class="vertical-middle">
            <td class="h3 project-title"><a href="/project/{{ $project['id'] }}">{{ $project['title'] }}</a></td>
            <td class="h4 budget">{{ $project['price_low'] }}€ - {{ $project['price_high'] }}€</div></td>
            <td><strong>{{ $project['first_name'] }} {{ $project['last_name'] }}</strong></td>
            <td><span class="date btn btn-xs btn-success{{ !$project['active'] ? ' hidden' : '' }}">{{ lang('active') }}</span><span class="date btn btn-xs btn-danger{{ $project['active'] ? ' hidden' : '' }}">{{ eeDate($project['bidding_ends_on']) }}</span></td>
            <td>{{ $project['project_duration'] }} päeva</td>
        </tr>
        <tr class="vertical-middle">
            <td colspan="5">
                <ul class="tag-cloud">
                    @foreach(explode(',', $project['tags']) as $tag)
                        <li><a class="btn btn-xs btn-default" href="#">{{ $tag }}</a></li>
                    @endforeach
                </ul>
            </td>
        </tr>
        @endforeach
        -->
        </tbody>
    </table>
    <div class="center gap">
        <button id="load-projects" class="btn btn-success" >{{ lang('button') }}</button>
    </div>
</div>
@endsection