@extends('base')
{{ setLangSource('faq') }}
@section('scripts')
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="/assets/js/faq.js"></script>
@endsection

@section('body')
    <section>

        <div class="container">


            <h3>{{ lang('title') }}</h3>

            <ul class="col-md-12 unstyled">
                <li>

                        <div id="kysim"></div>
                </li>

                <li>
                    <div>
                        <h4>{{ lang('who') }}</h4>
                        <p>{{ lang('about') }}</p>


                    </div>

                </li>
            </ul>
            <div  id="googleMap"  style="width:500px;height:380px;"></div>
        </div>
    </section>
@endsection