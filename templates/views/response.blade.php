@extends('base')
{{ setLangSource('response') }}
@section('body')

    <section  class="container">
        <h2>Makse detailid</h2>

        @if  ($fields["VK_SERVICE"] == "1101" && $ver == true)
            <table class="table">
                <tr>
                    <td>{{ lang('send') }}</td>
                    <td>{{ $fields['VK_SND_NAME'] }}</td>
                </tr>
                <tr>
                    <td>{{ lang('sendAcc') }}</td>
                    <td>{{ $fields['VK_SND_ACC'] }}</td>
                </tr>
                <tr>
                    <td>{{ lang('rec') }}</td>
                    <td>{{ $fields['VK_REC_NAME'] }}</td>
                </tr>
                <tr>
                    <td>{{ lang('recAcc') }}</td>
                    <td>{{ $fields['VK_REC_ACC'] }}</td>
                </tr>
                <tr>
                    <td>{{ lang('sum') }}</td>
                    <td>{{ $fields['VK_AMOUNT'] }}</td>
                </tr>
                <tr>
                    <td>{{ lang('date') }}</td>
                    <td>{{ $fields['VK_T_DATE'] }}</td>
                </tr>
                <tr>
                    <td>{{ lang('msg') }}</td>
                    <td>{{ $fields['VK_MSG'] }}</td>
                </tr>

            </table>
            @elseif($fields["VK_SERVICE"] != "1101")
            Mingi asi sellega läks metsa
            @elseif($ver == false)
            verify no go
            @endif





        <a href="/" class="btn btn-success" >{{ lang('nupp') }}</a>
    </section>
@endsection