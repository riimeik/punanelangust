@extends('base')

{{ setLangSource('project') }}

@section('body')
@include('alerts')
<div class="container blog panel-body">
    <div class="blog-item">
        <div class="blog-content">
            <div class="col-md-6">
                <h1><a href="">{{ $project['title'] }}</a></h1>
                <div class="entry-meta">
                    <div class="entry-meta">
                        <span><strong>{{ $project['first_name'] }} {{ $project['last_name'] }}</strong></span>|
                        <span>{{ eeDate($project['created_on']) }}</span>|
                        <span>{!! projectStatusButton($project) !!}</span>
                    </div>
                    <ul class="tag-cloud">
                    @foreach(explode(',', $project['tags']) as $tag)
                        <li><a class="btn btn-xs btn-default" href="#">{{ $tag }}</a></li>
                    @endforeach
                    </ul>
                </div>
                <table class="table table-responsive">
                    <tbody>
                    <tr>
                        <td><strong>{{ lang('deadline') }}</strong></td>
                        <td>{{ eeDate($project['bidding_ends_on']) }}</td>
                    </tr>
                    <tr>
                        <td><strong>{{ lang('time') }}</strong></td>
                        <td>{{ $project['project_duration'] }} {{ lang('days') }}</td>
                    </tr>
                    </tbody>
                </table>
                <div class="row center project-budget">
                    <div>
                        <p>{{ lang('budget') }}</p>
                        <h1>{{ $project['price_low'] }}€ - {{ $project['price_high'] }}€</h1>
                    </div>
                </div>
                <div class="row container">
                    <p>{{ $project['description'] }}</p>
                </div>
            </div>
            <hr class="visible-sm visible-xs" />
            <div class="col-md-6">
                <div class="row container">
                @if($isCreator)
                    @if($status == 'open')
                        <p class="center text-success">
                            <strong>{!! lang('desc-open') !!}</strong>
                        </p>
                    @elseif($status == 'pending-winner')
                        <p class="center text-warning">
                            <strong>{!! lang('desc-pending-winner') !!}</strong>
                        </p>
                    @elseif($status == 'pending-result')
                        <p class="center text-warning">
                            <strong>{!! lang('desc-pending-result') !!}</strong>
                        </p>
                        <br />
                        <form method="post" action="/project/{{ $project['id'] }}/finalize">
                            <div class="container center bid-button">
                                <button type="submit" class="btn btn-success btn-block">{{ lang('submit-finish') }}</button>
                            </div>
                        </form>
                    @else
                        <p class="center text-danger">
                            <strong>{!! lang('desc-finished') !!}</strong>
                        </p>
                    @endif
                @else
                    @if($status == 'open')
                    <form id="submit-bid-inline" class="form" method="post">
                        <div class="input-group" style="margin-bottom: 10px">
                            <input name="amount" required="required" type="number" min="{{ $project['price_low'] }}" max="{{ $project['price_high'] }}" class="form-control" placeholder="{{ lang('insert') }} {{ $project['price_low'] }} - {{ $project['price_high'] }}" />
                            <div class="input-group-addon">.00 €</div>
                        </div>
                        <textarea class="form-control " name="argument" rows="2" placeholder="{{ lang('textarea-placeholder') }}" required="required"></textarea>
                        <br />
                        <div class="container center bid-button">
                            <button type="submit" class="btn btn-success btn-block">{{ lang('submit') }}</button>
                        </div>
                    </form>
                    @elseif($status == 'pending-winner')
                        <p class="center text-warning">
                            <strong>{!! lang('desc-pending-winner-others') !!}</strong>
                        </p>
                    @elseif($status == 'pending-result')
                        <p class="center text-warning">
                            <strong>
                                @if($isWinner)
                                {!! lang('desc-pending-result-others-winner') !!}
                                @else
                                {!! lang('desc-pending-result-others') !!}
                                @endif
                            </strong>
                        </p>
                    @else
                        <p class="center text-danger">
                            <strong>{!! lang('desc-finished') !!}</strong>
                        </p>
                    @endif
                @endif
                </div>
                <hr />
                <div class="container">
                    <form id="accept-winner" method="post" action="/project/{{ $project['id'] }}/accept">
                        <input type="hidden" name="bid_id" />
                    </form>

                    <table id="bidders-list" class="table table-responsive{{ !$bids ? ' hidden' : '' }}">
                        <thead>
                            <tr>
                                <th>{{ lang('candidate') }}</th>
                                <th>{{ lang('price') }}</th>
                                <th>{{ lang('date') }}</th>
                                @if($isCreator && !$project['winning_bid'])
                                <th class="center">Võitja</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="hidden viewers-bid text-success">
                                <td></td>
                                <td></td>
                                <td></td>
                                @if($isCreator && !$project['winning_bid'])
                                <td class="center">
                                    <div><button class="choose-winner btn btn-info btn-xs">Määra &raquo;</button></div>
                                    <div style="display:none"><button class="confirm-winner btn btn-success btn-xs">Olen kindel!</button></div>
                                </td>
                                @endif
                            </tr>
                        @foreach($bids as $bid)
                            <tr id="bidder-row-{{ $bid['id'] }}" class="{{
                                $bid['id'] == $project['winning_bid'] ? 'winning-bid text-danger' : (
                                    ($bid['bidder'] == App::get('user')->get('id') ? 'viewers-bid text-success ' : '') .
                                    ($project['winning_bid'] ? 'non-winning-bid' : '')
                                )
                            }}
                            ">
                                <td>{{ $bid['first_name'] }} {{ $bid['last_name'] }}</td>
                                <td>{{ $bid['price'] }} €</td>
                                <td>{{ eeDate($bid['placed_on']) }}</td>
                                @if($isCreator && !$project['winning_bid'])
                                <td class="center">
                                    <div><button class="choose-winner btn btn-info btn-xs">Määra &raquo;</button></div>
                                    <div style="display:none"><button class="confirm-winner btn btn-success btn-xs">Olen kindel!</button></div>
                                </td>
                                @endif
                            </tr>
                            @if($isCreator)
                            <tr class="{{ $bid['id'] == $project['winning_bid'] ? 'winning-bid text-danger' : ($project['winning_bid'] ? 'non-winning-bid' : '') }}">
                                <td class="project-argument" colspan="4">&bull;&nbsp;&nbsp;{{ $bid['argument'] }}</td>
                            </tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                    <p id="no-bidders-message" class="center{{ $bids ? ' hidden' : '' }}">{{lang('nocandidates')}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection