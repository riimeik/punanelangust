@extends('base_old')

@section('body')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-4 ">
                    <img src="assets/img/profiil.png" alt="Profiilipilt"/>
                    <h3>muuda pilti</h3>
                    <input type="file" id="exampleInputFile">
                    <button type="submit" class="btn btn-default">Salvesta</button>
                </div>
                <div class="col-md-8">
                    <form class="form-horizontal" role="form">
                        <h4>Registreerimine</h4>
                        <div class="form-group">
                            <label class="col-md-8">Kasutajanimi</label>
                            <div class="col-md-8">
                                <input class="form-control"  type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-8">Eesnimi</label>
                            <div class="col-md-8">
                                <input class="form-control"type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-8">Perenimi</label>
                            <div class="col-md-8">
                                <input class="form-control"  type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-8">Email</label>
                            <div class="col-md-8">
                                <input class="form-control"  type="text">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-8 " >Parool</label>
                            <div class="col-md-8">
                                <input class="form-control" type="password">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-8 ">Parool uuesti</label>
                            <div class="col-md-8">
                                <input class="form-control" type="password">
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-md-7">
                                <input class="btn" value="Registreeru" type="button">
                            </div>
                        </div>
                </div>
                </form>
            </div>
        </div>
        </div>
    </section>


@endsection