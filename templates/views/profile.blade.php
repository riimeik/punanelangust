@extends('base')
{{ setLangSource('profile') }}
@section('body')
    <section>
        <div class="container">
            <div class="row">
            <div class="col-md-4 ">
                <img src="assets/img/profiil.png" alt="Profiilipilt"/>
                <h3>{{ lang('changepic') }}</h3>
                <input type="file" id="exampleInputFile">
                <button type="submit" class="btn btn-default">{{ lang('save') }}</button>
            </div>
                <div class="col-md-8">
                    <form class="form-horizontal">
                    <h4>{{lang('title')}}</h4>

                    <div class="form-group">
                        <label class="col-md-8">{{ lang('fname') }}</label>
                    <div class="col-md-8">
                        <input class="form-control" value="{{ App::get('user')->get('first_name') }}" type="text">
                    </div>
                    </div>
                    <div class="form-group">
                    <label class="col-md-8">{{ lang('lname') }}</label>
                    <div class="col-md-8">
                        <input class="form-control" value="{{ App::get('user')->get('last_name') }}" type="text">
                    </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-8">Email</label>
                        <div class="col-md-8">
                            <input class="form-control" value="{{ App::get('user')->get('email') }}" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-8 ">{{ lang('oldpwd') }}</label>
                        <div class="col-md-8">
                            <input class="form-control" type="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-8 " >{{ lang('newpwd') }}</label>
                        <div class="col-md-8">
                            <input class="form-control" type="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-8 ">{{ lang('newpwd') }} {{ lang('again') }}</label>
                        <div class="col-md-8">
                            <input class="form-control" type="password">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-7 ">{{ lang('pwdagain') }}</label>
                        <div class="col-md-7">
                            <input class="form-control" type="password">
                        </div>
                    </div>
                    <div class="form-group">

                        <div class="col-md-7">
                            <input class="btn" value={{ lang('save') }} type="button">
                        </div>
                    </div>

                </form>
            </div>
            </div>
        </div>
    </section>


@endsection