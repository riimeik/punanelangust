@extends('base')

{{ setLangSource('login') }}

@section('scripts')
    <script src="/assets/js/hex2base.js"></script>
    <script src="/assets/js/hwcrypto.min.js"></script>
@endsection

@section('body')
@include('alerts')
<section id="registration" class="container">
    <h3 class="center"> {{ lang('enter') }}</h3>
    <div class="container">
        <fieldset class="registration-form">
            <form class="center" method="post" action="/login">
                <div class="form-group">
                    <input type="text" name="email" placeholder="{{ lang('email') }}" class="form-control">
                </div>
                <div class="form-group">
                    <input type="password" name="password" placeholder="{{ lang('pw') }}" class="form-control">
                </div>
                <div class="form-group">
                    <button class="btn btn-success btn-md btn-block" type="submit">{{ lang('login') }}</button>
                </div>
            </form>

            <div id="alt-login-form" class="container">
                <form class="center " method="get" action="login/facebook">
                    <button class="btn btn-facebook">
                        <img class="logo" src="/assets/img/fb_logo.png" alt="fb-login" /> <span class="text">{{ lang('fb') }}</span>
                    </button>
                </form>
                <form id="idcard-login" class="center" method="post">
                    <textarea name="certificate" hidden="hidden"></textarea>
                    <textarea name="challenge" hidden="hidden">{{ getSession('idcardChallengeDigest') }}</textarea>
                    <textarea name="signature" hidden="hidden"></textarea>

                    <button class="btn btn-facebook">
                        <img class="logo" src="/assets/img/id_logo.png" alt="id-login" /> <span class="text">{{ lang('idcard') }}</span>
                    </button>
                </form>
            </div>

            <br /><br />
        </fieldset>
    </div>
</section>
@endsection