{{ setLangSource('project') }}



<projects xmlns="https://punanelangust.herokuapp.com">
    @foreach($projects as $project)
    <project id="{{ $project['id'] }}" title="{{ $project['title'] }}">
        <author id="{{ $project['creator'] }}">{{ $project['first_name'] . ' ' . $project['last_name'] }}</author>
        <price>
            <low>{{ $project['price_low'] }}€</low>
            <high>{{ $project['price_high'] }}€</high>
        </price>
        <dates>
            <created>{{ eeDate($project['created_on']) }}</created>
            <ends>{{ eedate($project['bidding_ends_on']) }}</ends>
            <duration>{{ $project['project_duration'] }} {{ lang('days') }}</duration>
        </dates>
        <active>{{ intval($project['active']) }}</active>
        <tags>
            @foreach(explode(',', $project['tags']) as $tag)
                <tag>{{ $tag }}</tag>
            @endforeach
        </tags>
    </project>
    @endforeach
</projects>