<?php

$et = [
    'title' => 'Viga',
    'heading' => 'Oi ei',
    'errnotfound' => 'Me ei suutnud serverist soovitud lehekülge leida.',
    'errdefault'=> 'Midagi läks nüüd viltu, sest server ei saanud päringust aru.',
    'back' => 'Mine esilehele'
];

$en = [
    'title' => 'Viga',
    'heading' => 'Oi ei',
    'errnotfound' => "We couldn't find the page you requested.",
    'errdefault'=> 'Something went wrong.',
    'back' => 'Back to home',
];

return [
    'et' => $et,
    'en' => $en
];