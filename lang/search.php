<?php

$et = [
    'search' => 'Otsing',
    'info'=>'Kõiki välju ei pea täitma',
    'title'=>'Pealkiri',
    'min'=>'Miinimum hind',
    'max'=>'Maksimum hind',
    'first'=> 'Eesnimi',
    'last'=>'Perekonnanimi',
    'search2' =>'Otsi'
];

$en = [
    'search' => 'Search',
    'info'=>'You don\'t need to fill all the forms',
    'title'=>'Title',
    'min'=>'Min fee',
    'max'=>'Max fee',
    'first'=> 'First name',
    'last'=>'Last name',
    'search2' => 'Search'
];

return [
    'et' => $et,
    'en' => $en
];