<?php

$et = [
    'send' => 'Maksja',
    'sendAcc'=>'Maksja konto',
    'rec' => 'Saaja',
    'recAcc' => 'Saaja konto',
    'sum' => 'Summa',
    'date' => 'Kuupäev',
    'msg' => 'Sõnum',
    'nupp' => 'Tagasi',
    'head' => 'Makse detailid',
    'success' => 'Ülekanne sooritati edukalt',
    'notverified' => 'Ülekanne tundub olevat ebakorrektne'
];

$en = [
    'send' => 'Sender',
    'sendAcc'=>'Sender account',
    'rec' => 'Receiver',
    'recAcc' => 'Receiver account',
    'sum' => 'Sum',
    'date' => 'Date',
    'msg' => 'Message',
    'nupp' => 'Back',
    'head' => 'Payment details',
    'success' => 'Transaction was successfully completed',
    'notverified' => 'Transaction couldn\'t be verified'
];

return [
    'et' => $et,
    'en' => $en
];