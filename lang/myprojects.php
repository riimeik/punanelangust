<?php

$et = [
    'new' => 'Loo uus projekt',
    'name' =>'Projekti nimi',
    'a_deadline'=>'Kandideerimise tähtaeg',
    's_deadline'=>'Esitamise tähtaeg',
    'category' =>'Kategooria',
    'min' =>'Miinimumhind',
    'max' =>'Maksimumhind',
    'info' => 'Lisainformatsioon',
    'all' => 'Kõik',
    'my_p' => 'Minu algatatud projektid',
    'cand' => 'Kandideeritavad',
    'now' => 'Käimasolev',
    'created' => 'Loodud',
    'bid'=> 'Pakkumine',
    'time' => 'Eeldatav ajakulu',
    'day' =>'päeva',
    'budget' =>'Eelarve',
    'creator' => 'Looja',
    'tags' => 'Märksõnad',
    'tags-helper' => 'Eralda märksõnad komadega, nt: käsitöö,vegan',
    'submitsuccess' => 'Uue projekti loomine õnnestus',
    'submiterror' => 'Projekti loomine ebaõnnestus - palun veendu, et kõik väljas sisaldavad korrektseid andmeid'
];

$en = [
    'new' => 'Create new project',
    'name' => 'Project name',
    'a_deadline'=>'Application deadline',
    's_deadline'=>'Submission deadline',
    'category' =>'Category',
    'min' =>'Minimum fee',
    'max' =>'Maximum fee',
    'info' => 'Additional information',
    'all' => 'All',
    'my_p' => 'My projects',
    'cand' => 'Projects I am candidate in ',
    'now' => 'Ongoing projects',
    'titleMy' => 'My projects',
    'created' => 'Created',
    'bid'=> 'Bid',
    'time' => 'Expected time to complete',
    'day' =>'days',
    'budget' =>'Budget',
    'creator' => 'Creator',
    'tags' => 'Tags',
    'tags-helper' => 'Comma-separated list of tags, e.g handicraft,vegan',
    'submiterror' => 'Project creation failed - please verify that all fields contain valid data',
    'submitsuccess' => 'Project creation was successful'
];

return [
    'et' => $et,
    'en' => $en
];