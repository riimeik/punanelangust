<?php

$et = [
    'more' => 'Vaata rohkem',
    'all' => 'Kuva kõik projektid',
    'popular' => 'Kõige populaarsemad projektid',
    'deadline' => 'Tähtaeg',
    'price' => 'Eelarve',
    'register' => 'Registeeri kasutajaks',
    'welcome' => 'Tere tulemast!'
];

$en = [
    'more' => 'More information',
    'all' => 'Show all projects',
    'popular' => 'Most popular projects',
    'deadline' => 'Deadline',
    'price' => 'Budget',
    'register' => 'Sign up',
    'welcome' => 'Welcome to our website!'
];

return [
    'et' => $et,
    'en' => $en
];