<?php

$et = [
    'home' => 'Avaleht',
    'projects' =>'Projektid',
    'search' => 'Otsing',
    'overview' => 'Ülevaade',
    'profile' => 'Profiil',
    'y_projects' => 'Sinu projektid',
    'messages'=> 'Sõnumid',
    'logout' => 'Logi välja',
    'whereami'=>'Kus ma olen?',
    'login' => 'Logi sisse',
    'reg' => 'Registeeri',
    'language' => 'Keel',
    'et' => 'eesti',
    'en' => 'inglise',
    'transactions' => 'Rahatehingud'
];

$en = [
    'home' => 'Home',
    'projects' => 'Projects',
    'search' => 'Search',
    'overview' => "Overview",
    'profile' => 'Profile',
    'y_projects' => 'Your projects',
    'messages' => 'Messages',
    'logout' => 'Logout',
    'whereami'=>'Where am I ?',
    'login' => 'Login',
    'reg' => 'Register',
    'language' => 'Language',
    'et' => 'Estonian',
    'en' => 'English',
    'transactions' => 'Transactions'
];

return [
    'et' => $et,
    'en' => $en
];