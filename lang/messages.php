<?php

$et = [
    'title' => 'Sõnumid',
    'author' => 'Autor',
    'when' => 'Millal'
];

$en = [
    'title' => 'Messages',
    'author' => 'Author',
    'when' => 'Sent on'
];

return [
    'et' => $et,
    'en' => $en
];