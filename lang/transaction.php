<?php

$et = [
    'title' => 'Tehingud',
    'submit' => 'Genereeri uus makselahendus',
    'insert-amount' => 'Sisesta summa',
    'choose-bank' => 'Vali pank',
    'bank' => 'Pangalink',
    'bank-transaction' => 'Kontole saab raha laadida läbi panga',
    'balance' => 'Konto saldo',
    'sender' => 'Saatja',
    'recipient' => 'Saaja',
    'sum' => 'Summa',
    'date' => 'Kuupäev',
    'history' => 'Tehingute ajalugu',
    'no-transactions' => 'Sinu kontoga ei ole seotud ühtegi ülekannet'

];

$en = [
    'title' => 'Transactions',
    'submit' => 'Generate new banklink payment',
    'insert-amount' => 'Enter a sum',
    'choose-bank' => 'Select bank',
    'bank' => 'Banklink',
    'bank-transaction' => 'Money can be transferred to your account by a bank transaction',
    'balance' => 'Balance',
    'sender' => 'Sender',
    'recipient' => 'Recipient',
    'sum' => 'Sum',
    'date' => 'Date',
    'history' => 'Transaction history',
    'no-transactions' => 'No transaction involving your account could be found'
];

return [
    'et' => $et,
    'en' => $en
];