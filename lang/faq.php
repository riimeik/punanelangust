<?php
//Heidi tõlkis
$et = [
    'question' => 'Tähtis küsimus nr',
    'answer' =>'Eesti: Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus',
    'about' => 'Simo Pähk, Heidi Urmet ja Krisseliine Pärt on Tartu Ülikooli informaatika tudengid. Peamiselt oleme oma veebirakendust arendanud Tartu Ülikooli
                        Matemaatika-Informaatika teaduskonna majas ning tihti peale võite meid leida sealt!',
    'who' => 'Kes me oleme?',
    'title' =>'Korduma kippuvad küsimused'
];

$en = [
    'question' => 'Important question nr',
    'answer' => 'English: Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo. Quisque sit amet est et sapien ullamcorper pharetra. Vestibulum erat wisi, condimentum sed, commodo vitae, ornare sit amet, wisi. Aenean fermentum, elit eget tincidunt condimentum, eros ipsum rutrum orci, sagittis tempus lacus enim ac dui. Donec non enim in turpis pulvinar facilisis. Ut felis. Praesent dapibus, neque id cursus faucibus, tortor neque egestas augue, eu vulputate magna eros eu erat. Aliquam erat volutpat. Nam dui mi, tincidunt quis, accumsan porttitor, facilisis luctus, metus',
    'about' =>'We are Kirsseliine Pärt, Heidi Urmet and Simo Pähk. We are computer science students at University of Tartu. Mainly we have been developing out wepapp at the Tartu University’s Faculty of Science and Technology building and oftentimes you can find us there!
You can contact us at punanelangust@gmail.com',
    'who' =>'Who are we?',
    'title' => 'Frequently asked questions'
];

return [
    'et' => $et,
    'en' => $en
];