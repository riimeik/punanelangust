<?php

$et = [
    'error' => 'Me ei suutnud serverist soovitud lehekülge leida.',
    'error2'=>'Midagi läks nüüd viltu, sest server ei saanud päringust aru.',

];

$en = [
    'error' => 'Page not found.',
    'error2'=>'Something went wrong.',

];

return [
    'et' => $et,
    'en' => $en
];