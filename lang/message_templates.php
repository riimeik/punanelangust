<?php

$et = [
    'new_bid' => '<strong>{{ $sender }}</strong> esitas sinu projektile <strong>{{ $project_title }}</strong> kandidatuuri suurusega <strong>{{ $bid_price }}€</strong>!',
    'project_payment_in' => 'Sinu kontole laekus summa <strong>{{ $price }}€</strong> projekti <strong>{{ $project_title }}</strong> ellu viimise eest.',
    'project_payment_out' => 'Sooritasid edukalt projekti <strong>{{ $project_title }}</strong> ellu viimise eest tasumise ettemakse summas <strong>{{ $price }}€</strong>. Kui koostöö osutub viljakaks, saab projekti valmides summa omanikuks <strong>{{ $recipient }}</strong>.'
];

$en = [
    'new_bid' => '<strong>{{ $sender }}</strong> placed a bid on your project <strong>{{ $project_title }}</strong> worth <strong>{{ $bid_price }}€</strong>!',
    'project_payment_in' => 'A payment of <strong>{{ $price }}€</strong> has been made to you for finishing the project <strong>{{ $project_title }}</strong>.',
    'project_payment_out' => 'A prepayment of <strong>{{ $price }}€</strong> by you for project <strong>{{ $project_title }}</strong> was successfully completed. If the project is carried out as expected, the sum will be re-transferred to  <strong>{{ $recipient }}</strong>.'
];

return [
    'et' => $et,
    'en' => $en
];