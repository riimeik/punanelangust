<?php

$et = [
    'menu' =>'Menüü',
    'home' => 'Koduleht',
    'where' => 'Kus ma olen?',
    'search' => 'Otsing',
    'projects' => 'Projektid',
    'personal' =>'Isiklikku',
    'register' => 'Registreeri',
    'login' => 'Logi sisse',
    'profile'=> 'Profiil',
    'myPro'=>'Minu projektid',
    'mes'=>'Sõnumid',
    'logOut'=>'Logi välja'

];

$en = [
    'menu' =>'Menu',
    'home' => 'Home page',
    'where' => 'Where am I?',
    'search' => 'Search',
    'projects' => 'Projects',
    'personal' =>'Personal',
    'register' => 'Register',
    'login' => 'Log in',
    'profile'=> 'Profile',
    'myPro'=>'My projects',
    'mes'=>'Messages',
    'logOut'=>'Log out'
];

return [
    'et' => $et,
    'en' => $en
];