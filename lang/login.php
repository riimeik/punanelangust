<?php

$et = [
    'enter' => 'Logimiseks sisesta oma andmed',
    'login' =>'Logi sisse',
    'fb' =>'Sisene läbi Facebooki',
    'idcard' => 'Sisene ID-kaardiga',
    'email' =>'Meiliaadress',
    'pw' => 'Parool',
    'nouser' => 'Sellise meiliaadressi ja parooliga kasutajat ei leitud',
    'nomail' => 'Rakendusel ei õnnestunud pärida sinu meiliaadressi',
    'failuser' => 'Kasutaja registreerimine ebaõnnestus',
    'faillogin' => 'Logimine ebaõnnestus',
    'mustlog' =>'Jätkamiseks pead sa kõigepealt sisse logima'
];

$en = [
    'enter' => 'Enter your data to log in',
    'login' => 'Login',
    'fb' => 'Login with Facebook',
    'idcard' => 'Login with ID-card',
    'email' => 'Email',
    'pw' => 'Password',
    'nouser' => 'This user was not found',
    'nomail' => 'Sorry, we did not get your email',
    'failuser' => 'Register failed',
    'faillogin' => 'Login failed',
    'mustlog' => 'You have to login to continue'
];

return [
    'et' => $et,
    'en' => $en
];