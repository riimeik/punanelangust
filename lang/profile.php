<?php

$et = [
    'title' => 'Muuda kontaktandmeid',
    'changepic' => 'Muuda pilti',
    'save'=>'Salvesta',
    'fname'=>'Eesnimi',
    'lname'=>'Perekonnanimi',
    'oldpwd'=>'Vana parool',
    'newpwd'=> 'Uus parool',
    'again'=>'uuesti',
    'pwdagain' =>'Sisesta parool muudatuste kinnitamiseks'
];

$en = [
    'title' => 'Change personal data',
    'changepic' => 'Change picture',
    'save'=>'Save',
    'fname'=>'First name',
    'lname'=>'Last name',
    'oldpwd'=>'Old password',
    'newpwd'=> 'New password',
    'again'=>'again',
    'pwdagain' => 'Enter the password to verify changes'
];

return [
    'et' => $et,
    'en' => $en
];