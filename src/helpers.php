<?php

global $app;

// Template'ide sees saab kasutada nt nii - App::get('logger')->log('..')
use Symfony\Component\HttpFoundation\JsonResponse;

class App {
    public static $app;

    public static function get($name = null) {
        if($name == null) {
            return self::$app;
        }
        
        return get(self::$app, $name);
    }
}

App::$app = $app;

function controller($name) {
    return "Langust\\Controller\\" . ucfirst($name);
}

// Saab küsida massiivi elementi ilma, et peaks ise kontrollima, kas element eksisteerib
function get($array, $key, $default = null) {
    if(is_array($key)) {
        $firstKey = array_shift($key);
        return (isset($array[$firstKey]) ? get($array[$firstKey], $key, $default) : $default);
    }

    return (isset($array[$key]) ? $array[$key] : $default);
}

// Flash sõnumid on ühekordseks kasutamiseks - kustutatakse kohe, kui loed
function addFlash($attribute, $value) {
    App::get('session')->getFlashBag()->set($attribute, $value);
}

// Abifunktsioon selleks, et flash küll requesti lõpuks ära kustutataks, aga et sama requesti ajal saaks seda korduvalt pärida
function getFlash($attribute, Array $default = []) {
    static $holders = [];

    if(!get($holders, $attribute)) {
        $holders[$attribute] = App::get('session')->getFlashBag()->get($attribute, $default);
    }

    return $holders[$attribute];
}

function consumeFlashAlerts($keys) {
    foreach($keys as $key) {
        App::get('session')->getFlashBag()->get($key);
    }
}

function setSession($attribute, $value) {
    App::get('session')->set($attribute, $value);
}

function getSession($attribute, $default = null) {
    return App::get('session')->get($attribute, $default);
}

function removeSession($attribute) {
    return App::get('session')->remove($attribute);
}

function eeDate($input) {
    $format = '%d %b, %H:%M';
    $input = strtotime($input);

    if(date('Y') != date('Y', $input)) {
        $format = '%d %b %Y, %H:%M';
    }

    $time = strftime($format, $input);

    if(substr($time, 0, 1) == '0') {
        $time = substr($time, 1);
    }

    return utf8_encode($time);
}

function json($data = []) {
    $alerts = [];

    if(!get($data, 'redirect')) {
        if(getFlash('success')) {
            $alerts['success'] = getFlash('success');
        }

        if(getFlash('error')) {
            $alerts['error'] = getFlash('error');
        }

        if($alerts) {
            $data['alerts'] = $alerts;
        }
    }
    
    return new JsonResponse($data);
}


function xml($file, $data = []) {
    $result = App::get('blade')->make('xml_' . $file, $data)->render();
    $result = html_entity_decode(preg_replace('/&(quot|amp|apos|lt|gt);/i', '&$1_tmp;', $result));
    $result = preg_replace('/&(quot|amp|apos|lt|gt)_tmp;/i', '&$1;', $result);
    
    return json(['xml' => $result, 'xsl' => $file]);
}

function getLangSource($file) {
    $defs = require __DIR__ . '/../lang/' . $file . '.php';

    return (get($defs, App::get('lang')) ?: $defs[array_keys($defs)[0]]);
}

$app['lang.defs'] = [];

function setLangSource($file) {
    if(!get(App::get()['lang.defs'], $file)) {
        App::get()['lang.defs'] = array_merge(App::get()['lang.defs'], [$file => getLangSource($file)]);
    }
    
    App::get()['lang.current'] = $file;
}

function tempLangSource($file = null) {
    static $stack = [];
    
    if($file) {
        $stack[] = App::get()['lang.current'];
        setLangSource($file);
    } else {
        setLangSource(array_pop($stack));
    }
}

function lang($id) {
    return get(get(App::get('lang.defs'), App::get('lang.current')), $id);
}

function url($route, $params = []) {
    $query = (App::get('query.persistent') ?: []) + $params;
    $parts = [];

    foreach($query as $key => $value) {
        $parts[] = $key . '=' . $value;
    }

    return $route . ($parts ? '?' . implode('&', $parts) : '');
}

function projectStatusButton($p, $size = "xs") {
    $status = projectStatus($p);
    $class = 'warning';
    
    if($status == 'open') {
        $class = 'success';
    } else if($status == 'finished') {
        $class = 'danger';
    }

    tempLangSource('project');
    $text = lang('status-' . $status);
    tempLangSource();
    
    return '<span class="btn btn-' . $size . ' btn-' . $class . '">' . $text . '</span>';
}

function projectStatus($p) {
    if($p['active']) {
        return 'open';
    } else if(!$p['winning_bid']) {
        return 'pending-winner';
    } else if(!$p['result_delivered_on']) {
        return 'pending-result';
    } else {
        return 'finished';
    }
}