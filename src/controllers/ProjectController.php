<?php

namespace Langust\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ProjectController implements ControllerProviderInterface {

    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];

        $controllers->get('/search', controller('ProjectController::showProjectSearch'));
        $controllers->post('/search', controller('ProjectController::showProjectSearchInfo'));

        $controllers->get('/', controller('ProjectController::showProjects'));

        $controllers->get('/{id}', controller('ProjectController::showProject'))
        ->assert('id', '\d+');

        $controllers->post('/{id}', controller('ProjectController::handleBid'))
        ->assert('id', '\d+')
        ->before($app['auth']->requiresPrivilege('USER'));

        $controllers->post('/{id}/accept', controller('ProjectController::chooseWinner'))
        ->assert('id', '\d+')
        ->before($app['auth']->requiresPrivilege('USER'));

        $controllers->post('/{id}/finalize', controller('ProjectController::finalizeProject'))
        ->assert('id', '\d+')
        ->before($app['auth']->requiresPrivilege('USER'));

        $controllers->get('/{id}/tags', controller('ProjectController::showTags'))
        ->assert('id', '\d+');

        $controllers->get('/myprojects', controller('ProjectController::newProject'))
        ->before($app['auth']->requiresPrivilege('USER'));

        $controllers->post('/myprojects', controller('ProjectController::showNewProject'))
        ->before($app['auth']->requiresPrivilege('USER'));

        return $controllers;
    }

    public function showProjects(Application $app, Request $request) {
        $start = ($request->get('start') ? intval($request->get('start', 10)) : 0);
        $end = ($request->get('end') ? intval($request->get('end', 10)) : 0);

        $projects = $app['db']->fetchAll('SELECT * FROM project_full LIMIT ' . ($end - $start) . ' OFFSET ' . $start, []);

        if(get($app, 'json')) {
            return xml('project_all', ['projects' => $projects]);
        }
        
        $data = [
            'title' => 'Projektid',
            'projects' => $projects
        ];

        return $app['blade']->make('project_all', $data)->render();
    }

    public function showProject(Application $app, Request $request, $id, $data = []) {
        $project = $app['db']->fetchAssoc('SELECT pf.*, pb.bidder AS winning_bidder FROM project_full pf LEFT JOIN project_bid pb ON pf.winning_bid = pb.id WHERE pf.id = ?', [$id]);

        if(!$project) {
            setLangSource('project');
            addFlash('error', lang('noproject'));

            throw new NotFoundHttpException();
        }

        $bids = $app['db']->fetchAll('SELECT * FROM project_bid WHERE project = ? ORDER BY is_winning_bid DESC, IF(bidder = ?, 0, 1), placed_on DESC', [$id, $app['user']->get('id')]);

        $data = [
            'title' => $project['title'],
            'project' => $project,
            'bids' => $bids,
            'isCreator' => $project['creator'] == $app['user']->get('id'),
            'isWinner' => $project['winning_bidder'] == $app['user']->get('id'),
            'status' => projectStatus($project)
        ] + $data;
   
        return $app['blade']->make('project', $data)->render();
    }

    public function chooseWinner(Application $app, Request $request, $id) {
        setLangSource('project');

        $project = $app['db']->fetchAssoc('SELECT * FROM project_full WHERE id = ?', [$id]);
        $correctBid = $app['db']->fetchAssoc('SELECT b.*, CONCAT(u.first_name, \' \', u.last_name) AS name FROM bid b LEFT JOIN user u ON b.bidder = u.id WHERE b.id = ? AND b.project = ?', [$request->get('bid_id'), $id]);

        if(!$project || !$correctBid) {
            throw new NotFoundHttpException();
        }

        if($project['creator'] != $app['user']->get('id') || projectStatus($project) != 'pending-winner') {
            throw new BadRequestHttpException();
        }

        if($app['user']->get('balance') < $correctBid['price'] || !$app['banklink']->insertTransaction($app['user']->get('id'), 1, $correctBid['price'])) {
            addFlash('error', lang('winner-choosing-failure-transaction'));
        } else {
            $app['message']->sendBySystem('project_payment_out', [
                'project_title' => $project['title'],
                'price' => $correctBid['price'],
                'recipient' => $correctBid['name']
            ], $project['creator']);
            
            if(!$app['db']->executeUpdate('UPDATE project SET winning_bid = ? WHERE id = ?', [$request->get('bid_id'), $id])) {
                addFlash('error', lang('winner-choosing-failure'));
            } else {
                addFlash('success', lang('winner-choosing-ok'));
            }
        }

        return $app->redirect('/project/' . $id);
    }

    public function finalizeProject(Application $app, Request $request, $id) {
        setLangSource('project');

        $project = $app['db']->fetchAssoc('SELECT * FROM project_full WHERE id = ?', [$id]);
     
        if(!$project) {
            throw new NotFoundHttpException();
        }

        if(!$project['winning_bid'] || $project['creator'] != $app['user']->get('id') || projectStatus($project) != 'pending-result') {
            throw new BadRequestHttpException();
        }

        $correctBid = $app['db']->fetchAssoc('SELECT * FROM bid WHERE id = ?', [$project['winning_bid']]);

        if(!$app['banklink']->insertTransaction(1, $correctBid['bidder'], $correctBid['price'])) {
            addFlash('error', lang('winner-choosing-failure-transaction'));
        } else {
            $app['message']->sendBySystem('project_payment_in', [
                'project_title' => $project['title'],
                'price' => $correctBid['price']
            ], $correctBid['bidder']);
            
            if(!$app['db']->executeUpdate('UPDATE project SET result_delivered_on = NOW() WHERE id = ?', [$id])) {
                addFlash('error', lang('finalizing-failure'));
            } else {
                addFlash('success', lang('finalizing-ok'));
            }
        }

        return $app->redirect('/project/' . $id);
    }

    public function handleBid(Application $app, Request $request, $id) {
        $response = function($data = []) use($app, $request, $id) {
            return (get($app, 'json') ? json($data) : $this->showProject($app, $request, $id, $data));
        };

        $project = $app['db']->fetchAssoc('SELECT * FROM project_full WHERE id = ?', [$id]);

        if(!$project) {
            setLangSource('project');
            addFlash('error', lang('noproject'));
            throw new NotFoundHttpException();
        }

        if(projectStatus($project) != 'open') {
            throw new BadRequestHttpException();
        }
        
        $amount = intval($request->get('amount'), 10);
        
        if($amount < $project['price_low'] || $amount > $project['price_high']) {
            setLangSource('project');
            addFlash('error', lang('falseprice'));
            return $response();
        }

        if(!$app['db']->executeUpdate('CALL bid_set(?, ?, ?, @bid_id)', [$id, $app['user']->get('id'), $amount])) {
            setLangSource('project');

            addFlash('error', lang('failure'));
            return $response();
        }

        $app['db']->executeUpdate('UPDATE bid SET argument = ? WHERE id = @bid_id', [$request->get('argument')]);

        $bid = $app['db']->fetchAssoc('SELECT * FROM project_bid WHERE id = @bid_id');

        $app['message']->sendBySystem('new_bid', [
            'sender' => $app['user']->getFullName(),
            'project_title' => $project['title'],
            'bid_price' => $amount
        ], $project['creator']);

        $data = [
            'meta' => [
                'id' => $bid['id']
            ],
            'row' => [
                $bid['first_name'] . ' ' . $bid['last_name'],
                $bid['price'] . ' €',
                eeDate($bid['placed_on'])
            ]
        ];

        setLangSource('project');
        addFlash('success', lang('success'));
        return $response($data);
    }

    public function showTags(Application $app, Request $request, $id) {
        return json([
            'tags' => $app['db']->fetchAll('SELECT t.name FROM project_tag pt LEFT JOIN tag t ON pt.tag = t.id WHERE project = ?', [$id])
        ]);
    }

    public function showProjectSearch(Application $app, Request $request) {
        $data = [

        ];

        return $app['blade']->make('search2', $data)->render();
    }


    public function newProject(Application $app, Request $request) {
        $id = $app['user']->get('id');

        $projektid = $app['db']->fetchAll('SELECT id, title, created_on, price_low,price_high, bidding_ends_on FROM project_full where creator= ?', [$id]);

        $bids = $app['db']->fetchAll('select p.id, p.title, p.creator, p.bidding_duration, p.project_duration, bid.price,p.first_name, 
p.last_name,p.bidding_ends_on from project_full as p, bid where p.id=bid.project  and bid.bidder= ?',[$id]);


        $data = [
            'riin' => $projektid,
            'bids' => $bids
        ];

        return $app{'blade'}->make('myprojects', $data)->render();
    }

    public function showNewProject(Application $app, Request $request) {
        $title = '';
        $desc = '';
        $price_low = '';
        $price_high = '';
        $diff = '';
        $diff2 = '';
        setLangSource('myprojects');

        if ($request->get('description') && $request->get('projectName') && $request->get('hindMin') && $request->get('hindMax') && $request->get('kuupäev') && $request->get('tags') && $request->get('kuupäev3')) {
            $title = $request->get('projectName');
            $price_low = $request->get('hindMin');

            if ($price_low > 0) {
                $price_high = $request->get('hindMax');
                $date1 = str_replace("/", "-", $request->get('kuupäev'));
                $aaa = date('d-m-Y', strtotime($date1));
                $now = date('d-m-Y');
                $re = date_create($aaa);
                $re1 = date_create($now);
                $diff = date_diff($re1, $re);

                $date2 = str_replace("/", "-", $request->get('kuupäev3'));
                $bbb = date('d-m-Y', strtotime($date2));
                $re2 = date_create($bbb);
                $diff2 = date_diff($re, $re2);
                //print_r($diff2);

                if ($diff2->invert == 0) {
                    $sql = "CALL project_set(0, ?, ?, ?, ?, ?, ?, ?, @project_id);";
                    $app['db']->executeUpdate($sql, [$title, $desc, $app['user']->get('id'), $diff->days, $diff2->days, $price_low, $price_high]);

                    $tags = explode(',', $request->get('tags'));
                    $insertTags = [];

                    $projId =  $app['db']->fetchAssoc('SELECT @project_id AS id')['id'];
                    
                    foreach($tags as $tag) {
                        $insertTags[] = $projId;
                        $insertTags[] = $tag;
                    }

                    $app['db']->executeUpdate('INSERT IGNORE INTO tag (name) VALUES ' .substr(str_repeat('(?),', count($tags)), 0, -1), $tags);
                    $app['db']->executeUpdate('INSERT INTO project_tag (project, tag) VALUES ' .substr(str_repeat('(?, (SELECT id FROM tag WHERE name = ?)),', count($tags)), 0, -1), $insertTags);

                    addFlash('success', lang('submitsuccess'));

                    return $app->redirect('/project/myprojects');
                } else {
                    addFlash('error', lang('submiterror'));
                }
            } else {
                addFlash('error', lang('submiterror'));
            }
        } else{
            addFlash('error', lang('submiterror'));
        }


        $id = $app['user']->get('id');
        $projektid = $app['db']->fetchAll('SELECT id, title, created_on, price_low,price_high, bidding_ends_on FROM project_full where creator= ?', [$id]);
        $bids = $app['db']->fetchAll('select p.id, p.title, p.creator, p.bidding_duration, p.project_duration, bid.price,p.first_name, 
p.last_name,p.bidding_ends_on from project_full as p, bid where p.id=bid.project  and bid.bidder= ?',[$id]);


        $data = [
            'riin' => $projektid,
            'bids' => $bids
        ];

        return $app{'blade'}->make('myprojects', $data)->render();
    }

    public function showProjectSearchInfo(Application $app, Request $request) {
        $tingimused = [];
        $sisendid = [];
        if ($request->get('min_price')) {
            $sisendid[] = $request->get('min_price');
            $tingimused[] = " price_low >= ?";
        }
        if ($request->get('max_price')) {
            $sisendid[] = $request->get('max_price');

            $tingimused[] = " price_high <= ?";}
         if($request->get('title')){
             $sisendid[] = '%'.$request->get('title').'%';
            $tingimused[] = " title LIKE ?";}
        if($request->get('first_name')){
            $sisendid[] = '%'.$request->get('first_name').'%';
            $tingimused[] = " u.first_name LIKE ?";}
        if($request->get('last_name')){

            $sisendid[] = '%'.$request->get('last_name').'%';
            $tingimused[] = " u.last_name LIKE ?";}






            if($tingimused == []){

                $projektid = [];


            }
            else{

                $query = "SELECT * FROM project p LEFT JOIN user u ON p.creator=u.id WHERE" . implode(" AND ", $tingimused);
                $projektid = $app['db']->fetchAll($query, $sisendid);

            }



        $data = [
            'projektid2' => $projektid
                ];


        // print_r($projektid);
        //print_r($tingimused);
        //print_r($sisendid);
        //  die();
        return $app{'blade'}->make('search2', $data)->render();
    }
}
