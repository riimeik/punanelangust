<?php

namespace Langust\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;


class LoginController implements ControllerProviderInterface {
    private $authTokenCookie;

    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];

        $controllers->get('/login', controller('loginController::loginPage'))
        ->before($app['auth']->prohibitsPrivilege('USER'))
        ->before(function() use($app) {
            setSession('idcardChallenge', microtime(true) . rand(0, pow(10, 6)));
            setSession('idcardChallengeDigest', openssl_digest(getSession('idcardChallenge'), 'sha256'));
        });

        $controllers->post('/login', controller('loginController::passwordLogin'))
        ->before($app['auth']->prohibitsPrivilege('USER'));

        $controllers->match('/login/facebook', controller('loginController::facebookLoginPrompt'))
        ->method('GET|POST')
        ->before($app['auth']->prohibitsPrivilege('USER'));

        // Siia suunab facebook pärast logimist tagasi
        $controllers->get('/login/facebook/callback', controller('loginController::facebookLogin'))
        ->before($app['auth']->prohibitsPrivilege('USER'))
        ->before(function() use($app) {
            if(!removeSession('fb.callback')) {
                return $app->redirect(url('/login'));
            }
        });

        $controllers->match('/login/idcard', controller('loginController::idcardLogin'))
        ->method('POST')
        ->before($app['auth']->prohibitsPrivilege('USER'));

        $controllers->get('/logout', controller('loginController::logout'))
        ->before($app['auth']->requiresPrivilege('USER', '/', null));

        return $controllers;
    }

    public function loginPage(Application $app, Request $request) {
        $data = [
            'title' => 'Logi sisse',
        ];

        return $app['blade']->make('login', $data)->render();
    }

    public function passwordLogin(Application $app, Request $request) {
        $user = $app['db']->fetchAssoc(
            'SELECT * FROM user_authable WHERE email = ? AND password_hash IS NOT NULL', [
                $request->get('email')
            ]
        );

        if(!$user || !password_verify($request->get('password'), $user['password_hash'])) {
            setLangSource('login');
            return $this->error($app, lang('nouser'));
        }

        if(!$this->verifyAuthToken($app, $user)) {
            setLangSource('login');
            return $this->error($app, lang('faillogin'));
        }

        return $this->success($app);
    }

    public function facebookLoginPrompt(Application $app, Request $request) {
        // Seda parameetrit kontrollime siis, kui fb tagasi meid suunab
        setSession('fb.callback', true);

        return $app->redirect($app['fb']->getRedirectLoginHelper()->getLoginUrl(
            $request->getUriForPath('/login/facebook'.url('/callback')), [
                'email' // Info, mida me FB-lt hiljem pärida tahame
            ]
        ));
    }

    public function facebookLogin(Application $app, Request $request) {
        $helper = $app['fb']->getRedirectLoginHelper();
        $fbToken = $helper->getAccessToken();

        $getFbUser = function() use($app, $fbToken) {
            return $app['db']->fetchAssoc(
                'SELECT * FROM user_authable WHERE access_token = ?', [
                    $fbToken
                ]
            );
        };

        $user = $getFbUser();

        if(!$user) {
            $app['fb']->setDefaultAccessToken($fbToken);

            $graphResponse = $app['fb']->get('/me?fields=first_name,last_name,email');

            $fbData = [
                'email' => get($graphResponse->getDecodedBody(), 'email'),
                'first_name' => get($graphResponse->getDecodedBody(), 'first_name'),
                'last_name' => get($graphResponse->getDecodedBody(), 'last_name')
            ];

            if(!$fbData['email']) {
                setLangSource('login');
                return $this->error($app, lang('nomail'));
            }

            $simpleUser = $app['db']->fetchAssoc('SELECT * FROM user_authable WHERE email = ?', [$fbData['email']]);

            if(!$simpleUser) {
                // Registreerime kasutaja
                if(!$app['db']->executeUpdate('CALL user_set(NULL, :email, :first_name, :last_name, @user_id)', $fbData)) {
                    setLangSource('login');
                    return $this->error($app, lang('failuser'));
                }

                $simpleUser = $app['db']->fetchAssoc('SELECT * FROM user WHERE id = @user_id', []);
            }

            if(!$app['db']->executeUpdate('CALL userdata_set(?, ?, NULL, NULL, NULL)', [$simpleUser['id'], $fbToken])) {
                setLangSource('login');
                return $this->error($app, lang('faillogin'));
            }

            $user = $getFbUser();
        }

        if(!$this->verifyAuthToken($app, $user)) {
            setLangSource('login');
            return $this->error($app, lang('faillogin'));
        }

        return $this->success($app);
    }
    
    public function idcardLogin(Application $app, Request $request) {
        setLangSource('login');
        
        $challenge = getSession('idcardChallenge');
        $certificate = $request->get('certificate');
        $certData = openssl_x509_parse($certificate);
        $signature = base64_decode($request->get('signature'));

        $verified = openssl_verify($challenge, $signature, $certificate, OPENSSL_ALGO_SHA256);

        if($verified !== 1) {
            setLangSource('login');
            return $this->error($app, lang('faillogin'));
        }

        $getIdcardUser = function() use($app, $certData) {
            return $app['db']->fetchAssoc(
                'SELECT * FROM user_authable WHERE pin = ?', [
                    $certData['subject']['serialNumber']
                ]
            );
        };

        $user = $getIdcardUser();

        if(!$user) {
            $data = [
                'first_name' => $this->capitalizedName($certData['subject']['GN']),
                'last_name' => $this->capitalizedName($certData['subject']['SN'])
            ];

            // Registreerime kasutaja
            if(!$app['db']->executeUpdate('CALL user_set(NULL, NULL, :first_name, :last_name, @user_id)', $data)) {
                return $this->error($app, lang('failuser'));
            }

            $user = $app['db']->fetchAssoc('SELECT * FROM user WHERE id = @user_id', []);
       
            if(!$app['db']->executeUpdate('CALL userdata_set(?, NULL, NULL, ?, ?)', [$user['id'], $certData['subject']['serialNumber'], $request->get('certificate')])) {
                return $this->error($app, lang('faillogin'));
            }

            $user = $getIdcardUser();
        }

        if(!$this->verifyAuthToken($app, $user)) {
            return $this->error($app, lang('faillogin'));
        }

        return $this->success($app);
    }

    private function capitalizedName($name) {
        return implode('-', array_map('ucfirst', explode('-', $name)));
    }

    public function logout(Application $app, Request $request) {
        $response = new RedirectResponse(url('/'));
        $response->headers->clearCookie('authToken');

        return $response;
    }

    private function verifyAuthToken($app, &$user) {
        // $user on andmebaasist tõmmatud objekt

        if(!$user['auth_token']) {
            $user['auth_token'] = hash('sha256', $user['email'] + get($user, 'password_hash', '') + get($user, 'access_token', '') + rand(0, pow(10, 6)));

            if(!$app['db']->executeUpdate('CALL user_auth_set(:id, :auth_token)', $user)) {
                return false;
            }
        }

        $this->authTokenCookie = new Cookie('authToken', $user['id'] . ':' . $user['auth_token']);

        return true;
    }

    private function error($app, $text) {
        addFlash('error', $text);

        return $app->redirect(url('/login'));
    }

    private function success($app) {
        $response = new Response('', 302, ['location' => getSession('lastUri') ?: url('/')]);
        $response->headers->setCookie($this->authTokenCookie);

        return $response;
    }
}