<?php

namespace Langust\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BanklinkController implements ControllerProviderInterface {
    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];

        $controllers->post('/request/{id}', controller('banklinkController::redirectRequest'))
        ->before($app['auth']->requiresPrivilege('USER'))
        ->assert('id', '\d+');

        $controllers->post('/response', controller('banklinkController::handleResponse'))
        ->bind('banklinkResponse');

        $controllers->get('/form', controller('banklinkController::generateForm'))
        ->before($app['auth']->requiresPrivilege('USER'));
        
        return $controllers;
    }

    public function generateForm(Application $app, Request $request) {
        $html = $app['banklink']->setupPayment(
            $app['banklink']->getBank($request->get('service')),
            1,
            $request->get('amount'),
            $app['user']->get('id') . ':' .$request->get('message')
        )->buildRequestHtml();
        
        $html = '<form style="display:none" method="post" action="' . getenv('BANKLINK_URL') . $request->get('service') . '">' . $html . '</form>';
        
        return new Response($html);
    }

    public function handleResponse(Application $app, Request $request) {
        $data = [];

        foreach($_POST as $key => $value) {
            if(substr($key, 0, 3) == 'VK_') {
                $data[$key] = $value;
            }
        }

        $transaction = $app['banklink']->handleResponse(get($data, 'VK_REC_ID'), $data);
        $status = $transaction->isSuccesful() && $app['banklink']->insertTransaction(
                null, explode(':', get($data, 'VK_MSG'))[0], get($data, 'VK_AMOUNT'), $data
        );

        setLangSource('response');

        if(!$status) {
            addFlash('error', lang('notverified'));
        } else {
            addFlash('success', lang('success'));
        }
        
        if(getFlash('banklinkRedirect')) {
            return $app->redirect(getFlash('banklinkRedirect')[0]);
        }

        $data =[
            'title' => 'Pangetehing',
            'fields' => $data,
            'ver' => $transaction->isSuccesful()
        ];

        return $app['blade']->make('response', $data)->render();
    }
}