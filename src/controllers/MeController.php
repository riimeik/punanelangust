<?php

namespace Langust\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Request;

class MeController implements ControllerProviderInterface {
    public function connect(Application $app) {
        $controllers = $app['controllers_factory'];

        $controllers->get('/message', controller('meController::showMessages'))
        ->before($app['auth']->requiresPrivilege('USER'));

        $controllers->get('/transaction', controller('meController::showTransactions'))
        ->before($app['auth']->requiresPrivilege('USER'));

        $controllers->get('/banklink/{bank}/{amount}', controller('meController::generatePayment'))
        ->before($app['auth']->requiresPrivilege('USER'));

        return $controllers;
    }

    public function showMessages(Application $app, Request $request) {
        setLangSource('messages');

        $messages = $app['db']->fetchAll('SELECT * FROM user_message WHERE recipient = ? ORDER BY IF(read_on IS NULL, 0, 1), sent_on DESC', [$app['user']->get('id')]);

        $data = [
            'title' => lang('title'),
            'messages' => $messages
        ];

        return $app['blade']->make('message', $data)->render();
    }

    public function showTransactions(Application $app, Request $request) {
        setLangSource('transaction');

        $transactions = $app['db']->fetchAll('SELECT * FROM user_transaction WHERE from_user = ? OR to_user = ? ORDER BY confirmed_on DESC', [$app['user']->get('id'), $app['user']->get('id')]);
        
        $transactions = array_map(function($trans) {
            $trans['fields'] = unserialize($trans['fields']);
            return $trans;
        }, $transactions);
        
        $data = [
            'title' => lang('title'),
            'transactions' => $transactions,
            'banklink' => $app['banklink']->setupPayment('Swedbank', 5, 50, 'heyy')
        ];

        addFlash('banklinkRedirect', '/transaction');

        return $app['blade']->make('transaction', $data)->render();
    }

    public function generatePayment(Application $app, Request $request, $bank, $amount) {
        $app['banklink']->setupPayment($bank, 1, $amount, 'General deposit')->buildRequestHtml();
    }
}