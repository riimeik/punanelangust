<?php

// env: production | test | local
// debug: true | false | none -> production (false), test (true), local(true)

global $app;

use Langust\Provider\AuthServiceProvider;
use Langust\Provider\BanklinkServiceProvider;
use Langust\Provider\BladeServiceProvider;
use Langust\Provider\FacebookServiceProvider;
use Langust\Provider\MessagingServiceProvider;
use Silex\Provider\DoctrineServiceProvider;
use Silex\Provider\MonologServiceProvider;
use Silex\Provider\SessionServiceProvider;
use Silex\Provider\UrlGeneratorServiceProvider;
use Symfony\Component\Debug\ErrorHandler;
use Symfony\Component\Debug\ExceptionHandler;

$locales = ['et' => 'et-EE', 'en' => 'en-US'];
$app['lang'] = isset($_GET['lang']) && isset($locales[strtolower($_GET['lang'])]) ? strtolower($_GET['lang']) : 'et';

setlocale(LC_ALL, $locales[$app['lang']]);
date_default_timezone_set('Europe/Tallinn');

if(getenv('APP_ENV') != 'production') {
    (new \Dotenv\Dotenv(__DIR__ . '/../', '.env.local'))->load();
    (new \Dotenv\Dotenv(__DIR__ . '/../', '.env.credentials'))->load();
}

// Loeb .env faili, väärtused saab kätte funktsiooniga getenv()
(new \Dotenv\Dotenv(__DIR__ . '/../'))->load();

$app['debug'] = (getenv('APP_DEBUG') ? getenv('APP_DEBUG') == 'true' : in_array(getenv('APP_ENV'), ['local']));

if($app['debug']) {
    ErrorHandler::register();
}

ExceptionHandler::register($app['debug']);

// Logimine
$app->register(new MonologServiceProvider(), [
    'monolog.logfile' => __DIR__ . '/../logs/log.txt',
    'monolog.name' => getenv('APP_NAME')
]);

// Template'id
$app->register(new BladeServiceProvider(), [
    'blade.path.views' => [
        __DIR__ . '/../templates/views/',
        __DIR__ . '/../templates/xml/'
    ],
    'blade.path.cache' => __DIR__ . '/../templates/cache/'
]);

$app->register(new DoctrineServiceProvider(), [
    'db.options' => [
        'url' => (getenv('JAWSDB_MARIA_URL') ?: getenv('DB_URL')),
        'charset' => 'utf8'
    ]
]);

$app['db']->exec("SET time_zone = '" . date('P') . "'");

$app->register(new SessionServiceProvider(), [
    'session.test' => getenv('APP_ENV') == 'test'
]);

$app->register(new AuthServiceProvider());

$app->register(new UrlGeneratorServiceProvider());

$app->register(new FacebookServiceProvider());

$app->register(new MessagingServiceProvider());

$app->register(new BanklinkServiceProvider());

if(!$app['debug']) {
    $app->error(function (\Exception $e, $code) use($app) {
        setLangSource('error');

        if(!getFlash('error')) {
            switch ($code) {
                case 404:
                    $text = lang('errnotfound');
                    break;
                default:
                    $text = lang('errdefault');
            }

            addFlash('error', $text);
        }

        if(get($app, 'json')) {
            return json();
        }

        return $app['blade']->make('error', ['title' => lang('title')])->render();
    });
} else if(get($app, 'json')) {
    return json();
}