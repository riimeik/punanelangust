<?php

global $app;

use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

$app->before(function(Request $request) use(&$app) {
    if(!getSession('retainAlerts')) {
        consumeFlashAlerts(['success', 'error']);
    }

    if(array_key_exists('json', $_GET) || array_key_exists('json', $_POST)) {
        $app['json'] = true;
    }

    $app['query.persistent'] = ($request->get('lang') ? ['lang' => $request->get('lang')] : []);
}, Application::EARLY_EVENT);

$app->after(function(Request $request, Response $response) use($app) {
    setSession('retainAlerts', false);
    
    if(in_array($response->getStatusCode(), [302])) {
        setSession('retainAlerts', true);
    }

    if(get($app, 'json')) {
        return null;
    }

    if(in_array($response->getStatusCode(), [302, 200]) && strpos($request->getUri(), '/login') === false && strpos($request->getUri(), '/logout') === false) {
        $app['session']->set('lastUri', $request->getUri());
    }
}, Application::LATE_EVENT);

$app->get('/', function() use($app) {
    $projects = $app['db']->fetchAll('SELECT * FROM project_full pf LEFT JOIN (SELECT project, COUNT(*) AS bid_count FROM bid GROUP BY project) AS bc ON pf.id = bc.project ORDER BY active, bid_count LIMIT 3');

    foreach($projects as &$project) {
        $project['bidding_ends_on'] = eeDate($project['bidding_ends_on']);
    }

    $data = [
        'projects' => $projects,
        'banklink' => $app['banklink']->setupPayment('Swedbank', 5, 50, 'heyy')
    ];

    return $app['blade']->make('home', $data)->render();
});

$app->get('/profile', function() use($app) {
    $data = [
        'title' => 'Profiiiiil',
    ];

    return $app['blade']->make('profile', $data)->render();
});

$app->get('/faq', function() use($app) {
    $data = [
        'title' => 'KKK',
    ];

    return $app['blade']->make('faq', $data)->render();
});

$app->get('/register', function() use($app) {
    $data = [
        'title' => 'Registreerimaaa',
    ];

    return $app['blade']->make('register', $data)->render();
});

$app->get('/search', function() use($app) {
    $data = [
        'title' => 'Punane langust',
        'projects' => []
    ];

    return $app['blade']->make('search', $data)->render();
});

$app->mount('/', new \Langust\Controller\LoginController());

$app->mount('/', new \Langust\Controller\MeController());

$app->mount('/project', new \Langust\Controller\ProjectController());

// Lihtsalt testimiseks :)
$app->get('/phpinfo', function() use($app) {
    return phpinfo();
})
->before($app['auth']->requiresPrivilege('ADMIN'));

$app->get('/log', function() use($app) {
    return $app->sendFile('../logs/log.txt');
});

$app->get('/wstest', function() use($app) {
    $app['message']->push('all', 'test', '1');
    return $app->abort(302, 'Weee');
})->before($app['auth']->requiresPrivilege('ADMIN'));