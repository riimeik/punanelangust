<?php

namespace Langust\Model;

class User {
    private $data;
    private $privileges = ['GUEST'];

    public function __construct($data = null) {
        $this->data = $data ?: [];

        if($this->get('id')) {
            $this->privileges[] = 'USER';
        }

        if($this->get('admin')) {
            $this->privileges[] = 'ADMIN';
        }
    }

    public function hasPrivilege($privilege) {
        return in_array($privilege, $this->privileges);
    }

    public function get($column, $default = null) {
        return get($this->data, $column, $default);
    }
    
    public function getFullName() {
        return $this->get('first_name') . ' ' . $this->get('last_name');
    }
}