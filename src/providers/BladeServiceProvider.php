<?php

namespace Langust\Provider;

use Philo\Blade\Blade;
use Silex\Application;
use Silex\ServiceProviderInterface;

class BladeServiceProvider implements ServiceProviderInterface {
    public function register(Application $app) {
        $app['blade'] = function() use($app) {
            
            return $app['blade.instance']->view();
        };
        
        $app['blade.instance'] = $app->share(function() use ($app) {
            return new Blade($app['blade.path.views'], $app['blade.path.cache']);
        });
    }

    public function boot(Application $app) {
    }
}