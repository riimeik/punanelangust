<?php

namespace Langust\Provider;

use Langust\Controller\BanklinkController;
use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\CssSelector\Exception\InternalErrorException;
use Symfony\Component\HttpFoundation\Request;

class BanklinkServiceProvider implements ServiceProviderInterface {
    private $app;

    private $validBanks = ['DanskeBank', 'Krediidipank', 'LHV', 'Nordea', 'SEB', 'Swedbank'];

    private $payments = [];
    private $paymentAccounts = [];
   
    public function __construct($app = null) {
        $this->app = $app;
    }

    public function register(Application $app) {
        $app['banklink'] = $app->share(function() use($app) {
            return new static($app);
        });
        
        $app->mount('/banklink', new BanklinkController());
    }

    public function boot(Application $app) {
    }

    public function getBank($service) {
        $id = array_search($service, array_map([$this, 'getService'], $this->validBanks));

        if($id === false) {
            throw new InternalErrorException();
        }

        return get($this->validBanks, $id);
    }

    private function getService($bank) {
        return strtolower($bank) . '-legacy';
    }
    
    private function preparePayment($bank, $account) {
        $data = $this->app['db']->fetchAssoc('SELECT * FROM banklink_data WHERE service = ? OR account = ?', [
            $this->getService($bank),
            $account
        ]);

        $bank = $bank ?: $this->getBank($data['service']);

        if(!in_array($bank, $this->validBanks)) {
            return false;
        }

        $file = fopen(__DIR__ . '/../../tmp/our_priv.pem', 'w');
        fwrite($file, $data['our_private_key']);
        $privKeyFile = realpath(stream_get_meta_data($file)['uri']);
        fclose($file);

        $file = fopen(__DIR__ . '/../../tmp/their_public.pem', 'w');
        fwrite($file, openssl_pkey_get_details(openssl_pkey_get_public($data['their_certificate']))['key']);
        $pubKeyFile = realpath(stream_get_meta_data($file)['uri']);
        fclose($file);

        $protocol = new \Banklink\Protocol\iPizza(
            $data['account'],
            getenv('BANKLINK_SELLERNAME'),
            getenv('BANKLINK_SELLERACCOUNTNUM'),
            $privKeyFile,
            $pubKeyFile,
            $this->app['request']->getSchemeAndHttpHost() . '/banklink/response',
            true
        );

        $serviceClass = '\\Banklink\\' . $bank;
        
        $this->payments[$bank] = new $serviceClass($protocol, false, getenv('BANKLINK_URL') . $this->getService($bank));
        $this->paymentAccounts[$data['account']] = $bank;
    }
    
    private function getPaymentByBank($bank) {
        if(!get($this->payments, $bank)) {
            $this->preparePayment($bank, null);
        }
        
        return $this->payments[$bank];
    }

    private function getPaymentByAccount($account) {
        if(!get($this->paymentAccounts, $account)) {
            $this->preparePayment(null, $account);
        }

        return $this->getPaymentByBank(get($this->paymentAccounts, $account));
    }
 
    public function setupPayment($bank, $id, $sum, $message) {
        return $this->getPaymentByBank($bank)->preparePaymentRequest($id, $sum, $message, 'EST', 'EUR');
    }

    public function handleResponse($account, $data) {
        return $this->getPaymentByAccount($account)->handleResponse($data);
    }

    public function insertTransaction($from, $to, $amount, $fields = null) {
        if($this->app['db']->executeUpdate('INSERT INTO transaction (from_user, to_user, amount, fields) VALUES (?, ?, ?, ?)', [$from, $to, $amount, serialize($fields)])) {
            return $this->app['db']->lastInsertId();
        }

        return false;
    }
}