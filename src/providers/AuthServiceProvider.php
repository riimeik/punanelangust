<?php

namespace Langust\Provider;

use Langust\Model\User;
use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AuthServiceProvider implements ServiceProviderInterface {
    private $app;

    public function __construct($app = null) {
        $this->app = $app;
    }

    public function register(Application $app) {
        $app['auth'] = $app->share(function() use($app) {
            return new static($app);
        });
    }

    public function boot(Application $app) {
        $app['user'] = $app->share(function() use($app) {
            $cookie = $app['request']->cookies->get('authToken');

            if(strpos($cookie, ':') !== false) {
                list($userId, $token) = explode(':', $cookie, 2);

                $userData = $app['db']->fetchAssoc(
                    'SELECT * FROM user_overview WHERE id = ? AND auth_token = ?', [
                        $userId, 
                        $token
                    ]
                );

                return new User($userData);
            }

            return new User();
        });
    }

    public function requiresPrivilege($privilege, $redirect = '/login', $message = 'mustlog') {
        return $this->validatePrivilege($privilege, $redirect, $message, true);
    }

    public function prohibitsPrivilege($privilege, $redirect = '/', $message = null) {
        return $this->validatePrivilege($privilege, $redirect, $message, false);
    }

    private function validatePrivilege($privilege, $redirect, $message, $require) {
        setLangSource('login');

        $message = ($message ? lang($message) : null);
        $redirect = url($redirect);

        $app = $this->app;

        return function() use($app, $privilege, $redirect, $message, $require) {
            if($require && !$app['user']->hasPrivilege($privilege) || !$require && $app['user']->hasPrivilege($privilege)) {
                
                if($message) {
                    addFlash('error', $message);
                }
                
                if($redirect) {
                    if(get($app, 'json')) {
                        return json(['redirect' => url($redirect)]);
                    }
                    
                    return $app->redirect($redirect);
                }
                
                throw new AccessDeniedHttpException();
            }
        };
    }
}