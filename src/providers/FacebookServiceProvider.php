<?php

namespace Langust\Provider;

use Facebook\Facebook;
use Facebook\PersistentData\PersistentDataInterface;
use Silex\Application;
use Silex\ServiceProviderInterface;

class FacebookServiceProvider implements ServiceProviderInterface, PersistentDataInterface {
    private $app;

    public function __construct($app = null) {
        $this->app = $app;
    }

    public function register(Application $app) {
        $app['fb'] = $app->share(function() use($app) {
            return new Facebook([
                'persistent_data_handler' => new static($app)
            ]);
        });
    }

    public function boot(Application $app) {
    }

    public function get($key) {
        $fbBag = $this->app['session']->get('fb');
        return (is_array($fbBag) ? ($fbBag[$key] ?: null) : null);
    }

    public function set($key, $value){
        $fbBag = $this->app['session']->get('fb');
        $this->app['session']->set('fb', array_merge(is_array($fbBag) ? $fbBag : [], [$key => $value]));
    }
}