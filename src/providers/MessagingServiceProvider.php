<?php

namespace Langust\Provider;

use Exception;
use Requests;
use Silex\Application;
use Silex\ServiceProviderInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class MessagingServiceProvider implements ServiceProviderInterface, EventSubscriberInterface {
    private $app;
    private $queue = [];

    public function __construct($app = null) {
        $this->app = $app;
    }

    public function register(Application $app) {
        $app['message'] = $app->share(function() use($app) {
            $provider = new static($app);
            $app['dispatcher']->addSubscriber($provider);
            return $provider;
        });
    }

    public function boot(Application $app) {
    }

    public function send($content, $data, $recipient, $sender = null) {
        $sender = ($sender === null ? $this->app['user']->get('id') : $sender);
        $data = $data !== null ? serialize($data) : null;

        if(!$this->app['db']->executeUpdate('CALL message_add(?, ?, ?, ?, @out_id)', [$sender, $recipient, $content, $data])) {
            return false;
        }

        if(!$messages = $this->app['db']->fetchAssoc('SELECT unread_messages FROM user_overview WHERE id = ?', [$recipient])['unread_messages']) {
            return false;
        }

        $this->push($recipient, 'messageCount', $messages);

        return true;
    }

    public function sendBySystem($template, $data, $recipient = null) {
        $recipient = ($recipient === null ? $this->app['user']->get('id') : $recipient);

        return $this->send($template, $data, $recipient, 1);
    }

    public function renderFromTemplate($template, $data) {
        tempLangSource('message_templates');

        $result = preg_replace_callback('/\{\{\s*\$([^}\s]+)\s*\}\}/', function($matches) use($data) {
            return get($data, $matches[1]);
        }, lang($template));

        tempLangSource();

        return $result;
    }

    // id: userid or 'all', str: event, array: content
    public function push($recipient, $event, $content) {
        if(!$recipient) {
            $recipient = 'all';
        }
        
        if(!isset($this->queue[$recipient])) {
            $this->queue[$recipient] = [];
        }
        
        $this->queue[$recipient][] = [
            'event' => $event,
            'content' => $content
        ];
    }

    public function onKernelFinishRequest(FinishRequestEvent $event) {
        $this->app['logger']->info('Flushing ' . count($this->queue). ' notifications');

        $data = [
            'authToken' => $this->app['db']->fetchAssoc("SELECT CONCAT(id, ':', auth_token) AS authToken FROM user WHERE id = 1")['authToken'],
            'messages' => $this->queue
        ];

        try {
            Requests::post(getenv('WSSERVER_URL'), [], json_encode($data));
        } catch(Exception $e) {
            $this->app['logger']->error("Notifications couldn't be flushed");
        }
    }

    public static function getSubscribedEvents() {
        return array(
            KernelEvents::FINISH_REQUEST => array('onKernelFinishRequest'),
        );
    }
}