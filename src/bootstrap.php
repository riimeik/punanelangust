<?php

global $app;

$app = new Silex\Application();

require __DIR__ . '/helpers.php';

require __DIR__ . '/services.php';

require __DIR__ . '/routes.php';

return $app;